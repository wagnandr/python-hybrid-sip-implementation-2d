"""
Module for assembling the local element matrices.
"""

import numpy as np


def create_affine_trafo(p0, p1, p2):
    """
    The affine transformation from the counterclockwise oriented reference element to arbitrary triangles.
    """
    B = np.array([
        [p1[0]-p0[0], p2[0]-p0[0]],
        [p1[1]-p0[1], p2[1]-p0[1]]])
    b = np.array(p0)
    return B, b


def create_affine_trafo_cw(p0, p1, p2):
    """
    The affine transformation from the clockwise oriented reference element to arbitrary triangles.
    """
    B = np.array([
        [p1[0]-p0[0], -p2[0]+p0[0]],
        [p1[1]-p0[1], -p2[1]+p0[1]]])
    b = np.array(p0)
    return B, b


def assemble_local_stiffness(p0, p1, p2, kappa):
    """
    This function assembles the terms
        kappa(x)*inner(grad(u), grad(v))*dx
    inside our domain.
    """
    B, b = create_affine_trafo(p0, p1, p2)
    J = abs(np.linalg.det(B))
    B_inv = np.linalg.inv(B)
    # int_value = 0.5*J*kappa   # 0.5 is the volume of the reference triangle!
    integrand = lambda x: kappa(np.matmul(B, x)+b)
    int_value = J*gauss_quadrature_triangle_2(integrand)
    X = int_value*np.matmul(B_inv, B_inv.transpose())
    # gradients on the reference triangle
    grad_p0 = np.array([-1, -1])
    grad_p1 = np.array([+1, 0])
    grad_p2 = np.array([0, +1])
    # utility function: calculate inner(grad(phi_i), grad(phi_j))*dx
    assemble = lambda grad_pi, grad_pj: np.dot(grad_pi, np.matmul(X, grad_pj))
    # TODO: Matrix is symmetric. Use this here.
    E = np.array([
        [assemble(grad_p0, grad_p0), assemble(grad_p0, grad_p1), assemble(grad_p0, grad_p2)],
        [assemble(grad_p1, grad_p0), assemble(grad_p1, grad_p1), assemble(grad_p1, grad_p2)],
        [assemble(grad_p2, grad_p0), assemble(grad_p2, grad_p1), assemble(grad_p2, grad_p2)],
    ])
    return E


def assemble_local_mass(p0, p1, p2):
    """
    This function assembles the terms
        u*v*dx
    inside our domain.
    """
    B, b = create_affine_trafo(p0, p1, p2)
    J = abs(np.linalg.det(B))
    E = J * np.array([
        [1/12., 1/24., 1/24.],
        [1/24., 1/12., 1/24.],
        [1/24., 1/24., 1/12.]])
    return E


def gauss_quadrature_triangle_2(f):
    """
    Gaussian quadrature function of order 2 on the counterclockwise oriented reference triangle.
    """
    # Quadrature weights are taken from
    #   Riviere, Discontinuous Galerkin Methods for Solving Elliptic and Parabolic Equations
    w = np.array([15/90, 15/90, 15/90])
    s = np.array([[2/3, 15/90],
                  [15/90, 15/90],
                  [15/90, 2/3]])
    return w[0]*f(s[0]) + w[1]*f(s[1]) + w[2]*f(s[2])


def assemble_local_mass_rhs(p0, p1, p2, f):
    """
    This function assembles the terms
        f*v*dx
    inside our domain.
    """
    # local basis function
    phi_0 = lambda x: 1-x[0]-x[1]
    phi_1 = lambda x: x[0]
    phi_2 = lambda x: x[1]
    B, b = create_affine_trafo(p0, p1, p2)
    J = abs(np.linalg.det(B))
    f_loc = lambda x: f(np.matmul(B, x) + b)
    return np.array([
        J*gauss_quadrature_triangle_2(lambda x: phi_0(x)*f_loc(x)),
        J*gauss_quadrature_triangle_2(lambda x: phi_1(x)*f_loc(x)),
        J*gauss_quadrature_triangle_2(lambda x: phi_2(x)*f_loc(x))
    ])


def assemble_local_jump_interior(p0, p1):
    """
    This function assembles the terms
        inner(jump(u,n), jump(v,n))*dS
    over the interior boundary.
    It assumes a vertex ordering of the form
        0             1
        +-------------+
        2             3
    where the indices above (0 and 1) are the local indices of the upper cell,
    and the indices below (2 and 3) are the local indices of the lower cell.

    The constants 1/3 and 1/6 are just a result of 
        int_0^1 s^2 ds = 1/3
    and 
        int_0^1 s*(1-s) ds = 1/6.
    """
    # get edge length
    e = np.linalg.norm(np.array(p0)-np.array(p1))
    return np.array([
        [e/3, e/6, -e/3, -e/6],
        [e/6, e/3, -e/6, -e/3],
        [-e/3, -e/6, e/3, e/6],
        [-e/6, -e/3, e/6, e/3],
    ])


def assemble_local_jump_avg_interior(p0, p1, p2, p3, kappa1, kappa2):
    """
    This function assembles the terms
        inner(jump(u,n), avg(kappa(x)*grad(v)))*dS
    over the interior boundaries.
    It assumes a vertex ordering of the form
          1  4
           ++
          /||\
         / || \
        +--++--+ 
       2  0  3  5
    where the indices on the left (0, 1 and 2) belong to the left cell and 
    the indices on the right (3, 4 and 5) are the local indices of the right cell,
    and we integrate over the shared edge.
    It is important that the first triangle is oriented counterclockwise and the second clockwise.
    This precondition is checked through assertions.

    kappa1 is kappa inside the left cell, kappa2 is kappa inside the right cell.
    """
    # we first execute the line integral
    # note that the gradients are constant and can be pulled out of the integral,
    # thus here is the only part, where we have to apply quadrature.
    phi_0 = lambda s: 1-s
    phi_1 = lambda s: s
    p0_v, p1_v = np.array(p0), np.array(p1)
    param = lambda s: (1-s)*p0_v+s*p1_v
    # the contributions due to the left cell:
    int_p0_kappa1 = gauss_quadrature_interval_3(lambda s: phi_0(s)*kappa1(param(s))) 
    int_p1_kappa1 = gauss_quadrature_interval_3(lambda s: phi_1(s)*kappa1(param(s)))
    # the contributions due to the right cell:
    int_p0_kappa2 = gauss_quadrature_interval_3(lambda s: phi_0(s)*kappa2(param(s)))
    int_p1_kappa2 = gauss_quadrature_interval_3(lambda s: phi_1(s)*kappa2(param(s)))
    # now we calculate n*grad(phi)
    # the contributions due to the left cell:
    p0_vec = np.array(p0)
    p1_vec = np.array(p1)
    edge = p1_vec - p0_vec 
    edge_length = np.linalg.norm(edge) 
    grad_p0 = np.array([-1, -1])
    grad_p1 = np.array([+1, 0])
    grad_p2 = np.array([0, +1])
    B_left, b = create_affine_trafo(p0, p1, p2)
    assert np.linalg.det(B_left) > 0 
    B_left_inv = np.linalg.inv(B_left)
    n = np.array([+edge[1], -edge[0]])/edge_length
    q_left = 0.5*edge_length*np.matmul(B_left_inv, n) # 0.5 from average!
    u_n_grad_v_left_0 = np.dot(grad_p0, q_left)     # corresponds to the 0th vertex dof
    u_n_grad_v_left_1 = np.dot(grad_p1, q_left)     # corresponds to the 1st vertex dof
    u_n_grad_v_left_2 = np.dot(grad_p2, q_left)     # corresponds to the 2nd vertex dof
    # the contributions due to the right cell:
    grad_p0 = np.array([-1, +1])
    grad_p1 = np.array([+1, 0]) 
    grad_p2 = np.array([0, -1]) 
    n = np.array([-edge[1], +edge[0]])/edge_length
    B_right, _ = create_affine_trafo_cw(p0, p1, p3)
    assert np.linalg.det(B_right) > 0 
    B_right_inv = np.linalg.inv(B_right)
    q_right = 0.5*edge_length*np.matmul(B_right_inv, n) # 0.5 from average!
    u_n_grad_v_right_0 = np.dot(grad_p0, q_right)   # corresponds to the 3rd vertex dof
    u_n_grad_v_right_1 = np.dot(grad_p1, q_right)   # corresponds to the 4th vertex dof
    u_n_grad_v_right_2 = np.dot(grad_p2, q_right)   # corresponds to the 5th vertex dof
    # collect contributions
    return np.array([
        [+int_p0_kappa1*u_n_grad_v_left_0, +int_p1_kappa1*u_n_grad_v_left_0, 0, -int_p0_kappa1*u_n_grad_v_left_0, -int_p1_kappa1*u_n_grad_v_left_0, 0],
        [+int_p0_kappa1*u_n_grad_v_left_1, +int_p1_kappa1*u_n_grad_v_left_1, 0, -int_p0_kappa1*u_n_grad_v_left_1, -int_p1_kappa1*u_n_grad_v_left_1, 0],
        [+int_p0_kappa1*u_n_grad_v_left_2, +int_p1_kappa1*u_n_grad_v_left_2, 0, -int_p0_kappa1*u_n_grad_v_left_2, -int_p1_kappa1*u_n_grad_v_left_2, 0],
        [-int_p0_kappa2*u_n_grad_v_right_0, -int_p1_kappa2*u_n_grad_v_right_0, 0, +int_p0_kappa2*u_n_grad_v_right_0, +int_p1_kappa2*u_n_grad_v_right_0, 0],
        [-int_p0_kappa2*u_n_grad_v_right_1, -int_p1_kappa2*u_n_grad_v_right_1, 0, +int_p0_kappa2*u_n_grad_v_right_1, +int_p1_kappa2*u_n_grad_v_right_1, 0],
        [-int_p0_kappa2*u_n_grad_v_right_2, -int_p1_kappa2*u_n_grad_v_right_2, 0, +int_p0_kappa2*u_n_grad_v_right_2, +int_p1_kappa2*u_n_grad_v_right_2, 0],
    ])


def gauss_quadrature_interval_3(f):
    """
    Gaussian quadrature function of order 3 on the interval [0, 1].
    """
    # Quadrature weights are taken from
    #   Riviere, Discontinuous Galerkin Methods for Solving Elliptic and Parabolic Equations
    # for the interval (-1, +1).
    #   w = [5/9, 8/9, 5/9]
    #   s = [-0.774596669241, 0, +0.774596669241]
    # Converted to (0, +1) we get
    w = [5/9/2, 8/9/2, 5/9/2]
    s = [0.5*(-0.774596669241+1), 0.5*(0+1), 0.5*(+0.774596669241+1)]
    return w[0]*f(s[0]) + w[1]*f(s[1]) + w[2]*f(s[2])


def assemble_local_minus_jump_avg_sym_interior(p0, p1, p2, p3, kappa1, kappa2):
    """
    This function assembles the terms
        - inner(jump(u,n), avg(kappa(x)*grad(v)))*dS - inner(avg(kappa(x)*grad(u)), jump(v,n))*dS
    over the interior boundaries.
    The same preconditions as in assemble_local_jump_avg_interior are needed.
    """
    K = assemble_local_jump_avg_interior(p0, p1, p2, p3, kappa1, kappa2)
    return -K -K.transpose()


def cell_diameter(p0, p1, p2):
    """
    Calculates the cell diameter, i.e. the largest distance between 2 point in the triangle.
    """
    d1 = np.linalg.norm(p0-p1)
    d2 = np.linalg.norm(p0-p2)
    d3 = np.linalg.norm(p1-p2)
    return max(d1, d2, d3)


def calculate_weight(p0, p1, p2, p3):
    """
    Calculates the weight for our penalty term.
    """
    p0 = np.array(p0)
    p1 = np.array(p1)
    p2 = np.array(p2)
    p3 = np.array(p3)
    h1 = cell_diameter(p0, p1, p2)
    h2 = cell_diameter(p0, p1, p3)
    h = 0.5*(h1+h2) 
    p = 1   # polynomial degree. here P1
    return 3*5/h


def calculate_cot_theta(p0, p1, p2):
    """
    Calculates cot(theta) for the smallest angle in the triangle.
    """
    e01, e02, e12 = p0-p1, p0-p2, p1-p2
    le01, le02, le12 = np.linalg.norm(e01), np.linalg.norm(e02), np.linalg.norm(e12) 
    costheta0 = np.dot(e01, e02)/le01/le02
    costheta1 = -np.dot(e01, e12)/le01/le12
    costheta2 = np.dot(e02, e12)/le02/le12
    costheta_max = np.max([costheta0, costheta1, costheta2])
    if costheta0 == costheta_max:
        sintheta = np.linalg.norm(np.cross(e01, e02))/le01/le02
        return costheta_max/sintheta
    if costheta1 == costheta_max:
        sintheta = np.linalg.norm(np.cross(e01, e12))/le01/le12
        return costheta_max/sintheta
    if costheta2 == costheta_max:
        sintheta = np.linalg.norm(np.cross(e02, e12))/le02/le12
        return costheta_max/sintheta


def calculate_weight_exact(p0, p1, p2, p3, kappa_min_1, kappa_max_1, kappa_min_2, kappa_max_2):
    """
    Exact calculation of the weight by eq 2.38 in Riviere.
    """
    p0 = np.array(p0)
    p1 = np.array(p1)
    p2 = np.array(p2)
    p3 = np.array(p3)
    h1 = cell_diameter(p0, p1, p2)
    h2 = cell_diameter(p0, p1, p3)
    h = 0.5*(h1+h2) 
    cottheta_1 = calculate_cot_theta(p0, p1, p2)
    cottheta_2 = calculate_cot_theta(p0, p1, p3)
    # cottheta_1 = 1 
    # cottheta_2 = 1 
    p = 1   # polynomial degree. here P1
    return p*(p+1)*3/2*(kappa_max_1**2/kappa_min_1*cottheta_1+kappa_max_2**2/kappa_min_2*cottheta_2)/h


def extend(matrix):
    """
    Extends a small 4x4 matrix to a larger 6x6 matrix by padding the rows 2 and 5 and cols 2 and 5 with zeros.
    """
    assert matrix.shape[0] == 4 and matrix.shape[1] == 4
    extension = np.zeros((6,6))
    extension[0:2, 0:2] = matrix[0:2, 0:2]
    extension[3:5, 3:5] = matrix[2:4, 2:4]
    extension[3:5, 0:2] = matrix[2:4, 0:2]
    extension[0:2, 3:5] = matrix[0:2, 2:4]
    return extension


def assemble_local_boundary_integrals_interior(
    p0, p1, p2, p3, 
    kappa1, kappa2, 
    kappa_min1, kappa_max1, kappa_min2, kappa_max2
):
    """
    Assembles the terms
        - inner(jump(u,n), avg(kappa(x)*grad(v)))*dS - inner(avg(kappa(x)*grad(u)), jump(v,n))*dS
        + p*(p+1)/avg(h)*inner(jump(u,n), jump(v,n))*dS
    over the interior boundaries, where p=1 is the polynomial degree.
    The same preconditions as in assemble_local_jump_avg_interior are needed.
    kappa_min is a lower bound for kappa, and kappa_max an upper bound
    """
    K1 = assemble_local_jump_interior(p0, p1)
    K2 = assemble_local_minus_jump_avg_sym_interior(p0, p1, p2, p3, kappa1, kappa2)
    sigma = calculate_weight_exact(p0, p1, p2, p3, kappa_min1, kappa_max1, kappa_min2, kappa_max2)
    return extend(sigma*K1) + K2


def gauss_quadrature_triangle_5(f):
    """
    Gaussian quadrature function of order 5 on the counterclockwise oriented reference triangle.
    """
    # Quadrature weights are taken from
    #   Riviere, Discontinuous Galerkin Methods for Solving Elliptic and Parabolic Equations
    w = [0.112500000000,
         0.062969590272,
         0.062969590272,
         0.062969590272,
         0.066197076394,
         0.066197076394,
         0.066197076394]
    s = [[0.333333333333, 0.333333333333],
         [0.101286507323, 0.101286507323],
         [0.797426985353, 0.101286507323],
         [0.101286507323, 0.797426985353],
         [0.470142064105, 0.470142064105],
         [0.059715871789, 0.470142064105],
         [0.470142064105, 0.059715871789]]
    t = [weight*f(point) for weight, point in zip(w, s)]
    return sum(t)


def assemble_local_error_norm_squared(p0, p1, p2, f, u0, u1, u2):
    """
    Assembles 
        int (f - u)**2 dx
    on the triangle defined by the points p0, p1 and p2 with a 5th order quadrature scheme.
    """
    phi_0 = lambda x: 1-x[0]-x[1]
    phi_1 = lambda x: x[0]
    phi_2 = lambda x: x[1]
    B, b = create_affine_trafo(p0, p1, p2)
    J = abs(np.linalg.det(B))
    
    def g(x):
        val = u0*phi_0(x)+u1*phi_1(x)+u2*phi_2(x)-f(np.matmul(B, x)+b)
        return val**2
    
    int_val = gauss_quadrature_triangle_5(g)

    return J*int_val
