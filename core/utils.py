import csv
import matplotlib
from math import sqrt 


def set_width_in_percent(width_percent=1, height_percent=1, pgf=True):
    col_width = 455.24408                   # Get this from LaTeX using \showthe\columnwidth
    fig_width_pt = width_percent*col_width                    
    inches_per_pt = 1.0/72.27               # Convert pt to inch
    golden_mean = (sqrt(5)-1.0)/2.0         # Aesthetic ratio
    fig_width = fig_width_pt*inches_per_pt  # width in inches
    fig_height = height_percent*fig_width*golden_mean      # height in inches
    fig_size =  [fig_width,fig_height]

    if pgf:
        matplotlib.use("pgf")

    matplotlib.rcParams.update({
        "pgf.texsystem": "pdflatex",
        'font.family': 'serif',
        'text.usetex': True,
        'pgf.rcfonts': False,
        'figure.figsize': fig_size
    })


def load_column(file_path, column_name):
    data_list = []
    with open(file_path) as f:
        reader = csv.DictReader(f)
        for row in reader:
            data_list.append(float(row[column_name]))
    return data_list
