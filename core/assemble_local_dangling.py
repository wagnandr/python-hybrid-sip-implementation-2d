"""
Module for assembling the local element matrices for elements with dangling nodes.
"""

import numpy as np

from core.assemble_local import (
    create_affine_trafo, 
    create_affine_trafo_cw,
    gauss_quadrature_interval_3,
    cell_diameter,
    calculate_cot_theta
)


def assemble_local_jump_interior_dangling(list_p):
    """
    This function assembles the terms
        inner(jump(u,n), jump(v,n))*dS
    over the interior boundary.
    We assume that one cell (the upper one) consists of 2 points, while the lower one, consists of n-2 points.
        0             1
        +-+-+-+-+-+-+-+
        2 3 4         n
    These points are passed through list_p.
    """
    assert np.allclose(list_p[0], list_p[2]), "the first upper and lower point must be equal"
    assert np.allclose(list_p[1], list_p[-1]), "the last upper and lower point must be equal"
    assert len(list_p) >= 4, "function needs at least two points to work"
    list_p = [np.array(p) for p in list_p]  # numpyify the list
    num_points_upper_cell = 2
    num_points_lower_cell = len(list_p) - num_points_upper_cell
    K_loc = np.zeros((len(list_p), len(list_p)))
    num_intervals = num_points_lower_cell-1 
    e = np.linalg.norm(np.array(list_p[0])-np.array(list_p[1]))
    e_small = e/num_intervals
    # TODO: THIS ASSERT IS EXPENSIVE. It should be able to be switched off
    for idx in range(0, num_intervals):
        assert np.isclose(np.linalg.norm(list_p[idx+0+2]-list_p[idx+1+2]), e_small), "the lower edges have to be partitioned equally"
    K_loc[1, 1] = K_loc[0, 0] = e/3
    K_loc[0, 1] = K_loc[1, 0] = e/6
    # We use the 3rd order exact Simpson rule
    #   1/6*(f(0) + 4*f(0.5) + f(1))
    # to evaluate the other integrals
    for interval_idx in range(num_intervals):
        # left point of the interval:
        lp = interval_idx/num_intervals 
        left_idx = interval_idx+2
        # right point of the interval:
        rp = (interval_idx+1)/num_intervals
        right_idx = (interval_idx+1)+2
        # middle point
        mp = 0.5*(lp+rp)
        # fill in the integrals
        # these are negative, since contributions from different macro cells:
        K_loc[0, left_idx] += -e_small/6*(1*(1-lp) + 4*0.5*(1-mp) + 0*(1-rp))
        K_loc[left_idx, 0] += -e_small/6*(1*(1-lp) + 4*0.5*(1-mp) + 0*(1-rp))
        K_loc[0, right_idx] += -e_small/6*(0*(1-lp) + 4*0.5*(1-mp) + 1*(1-rp)) 
        K_loc[right_idx, 0 ] += -e_small/6*(0*(1-lp) + 4*0.5*(1-mp) + 1*(1-rp)) 
        K_loc[1, left_idx] += -e_small/6*(1*lp + 4*0.5*mp + 0*rp)
        K_loc[left_idx, 1] += -e_small/6*(1*lp + 4*0.5*mp + 0*rp)
        K_loc[right_idx, 1] += -e_small/6*(0*lp + 4*0.5*mp + 1*rp)
        K_loc[1, right_idx] += -e_small/6*(0*lp + 4*0.5*mp + 1*rp)
        # these are positive, since contributions from the same macro cell:
        K_loc[right_idx, right_idx] += e_small/3
        K_loc[left_idx, left_idx] += e_small/3
        K_loc[left_idx, right_idx] += e_small/6
        K_loc[right_idx, left_idx] += e_small/6
    return K_loc


def assemble_local_jump_avg_interior_dangling(list_p, kappa1, kappa2):
    """
    This function assembles the terms
        inner(jump(u,n), avg(kappa(x)*grad(v)))*dS
    over the interior boundaries.
    The the points of the upper cell should be ordered as follows
      2 +-\___
        |     \___
        |         \___
        |             \
      0 +--------------+ 1
    while the points on the lower cell should have the following layout
           4  5  6  7  8
      3 +--+--+--+--+--+
         \/ \/ \/ \/ \/
          9 10 11 12 13
    The assembly algorithm assumes that the triangles of the lower cell are equivalent modulo a translation.
    """
    assert np.allclose(list_p[0], list_p[3]), "the first upper and lower point must be equal"
    assert len(list_p) >= 4, "function needs at least two points to work"
    list_p = [np.array(p) for p in list_p]  # numpyify the list
    num_points_upper_cell = 3
    num_points_lower_cell = len(list_p) - num_points_upper_cell
    num_points_lower_edge = int((num_points_lower_cell-1)/2+1)
    assert np.allclose(list_p[1], list_p[2+num_points_lower_edge]), "the last upper and lower point must be equal"
    num_intervals = num_points_lower_edge-1
    # prepare upper cell:
    edge = list_p[1] - list_p[0]
    edge_length = np.linalg.norm(edge) 
    edge_length_small = edge_length/num_intervals
    grad_p0_upper = np.array([-1, -1])
    grad_p1_upper = np.array([+1, 0])
    grad_p2_upper = np.array([0, +1])
    B_upper, _ = create_affine_trafo(list_p[0], list_p[1], list_p[2])
    assert np.linalg.det(B_upper) > 0 
    B_upper_inv = np.linalg.inv(B_upper)
    # our normal, pointing from the upper cell to the lower cell:
    n_upper = np.array([+edge[1], -edge[0]])/edge_length
    q_upper = 0.5*np.matmul(B_upper_inv, n_upper) # 0.5 from average!
    n_grad_v_upper_0 = np.dot(grad_p0_upper, q_upper)     # corresponds to the 0th vertex dof
    n_grad_v_upper_1 = np.dot(grad_p1_upper, q_upper)     # corresponds to the 1st vertex dof
    n_grad_v_upper_2 = np.dot(grad_p2_upper, q_upper)     # corresponds to the 2nd vertex dof
    # prepare lower cell:
    grad_p0_lower = np.array([-1, +1])
    grad_p1_lower = np.array([+1, 0]) 
    grad_p2_lower = np.array([0, -1]) 
    # here we assume that all cells have the same shape!
    B_lower, _ = create_affine_trafo_cw(list_p[0+3], list_p[1+3], list_p[0+3+num_points_lower_edge])
    assert np.linalg.det(B_lower) > 0
    # TODO: THIS ASSERT IS EXPENSIVE. It should be able to be switched off
    for idx in range(1, num_intervals):
        B_lower_, _ = create_affine_trafo_cw(list_p[idx+0+3], list_p[idx+1+3], list_p[idx+0+3+num_points_lower_edge])
        assert np.allclose(B_lower, B_lower_), "lower triangles have to be equivalent"
    B_lower_inv = np.linalg.inv(B_lower)
    n_lower = np.array([-edge[1], +edge[0]])/edge_length
    q_lower = 0.5*np.matmul(B_lower_inv, n_lower) # 0.5 from average!
    n_grad_v_lower_0 = np.dot(grad_p0_lower, q_lower)     # corresponds to the (n+3)th vertex dof
    n_grad_v_lower_1 = np.dot(grad_p1_lower, q_lower)     # corresponds to the (n+1+3)th vertex dof
    n_grad_v_lower_2 = np.dot(grad_p2_lower, q_lower)     # corresponds to the (n+3+#points-on-lower-edge)th vertex dof
    # start assembling our local element matrix:
    K_loc = np.zeros((len(list_p), len(list_p)))
    # beginn with the upper cell
    phi_0_upper = lambda s: 1-s
    phi_1_upper = lambda s: s
    param = lambda s: (1-s)*list_p[0]+s*list_p[1]
    int_p0_kappa1_upper = edge_length*gauss_quadrature_interval_3(lambda s: phi_0_upper(s)*kappa1(param(s))) 
    int_p1_kappa1_upper = edge_length*gauss_quadrature_interval_3(lambda s: phi_1_upper(s)*kappa1(param(s)))
    K_loc[0:3, 0:3] = np.array([
        [+int_p0_kappa1_upper*n_grad_v_upper_0, +int_p1_kappa1_upper*n_grad_v_upper_0, 0],
        [+int_p0_kappa1_upper*n_grad_v_upper_1, +int_p1_kappa1_upper*n_grad_v_upper_1, 0],
        [+int_p0_kappa1_upper*n_grad_v_upper_2, +int_p1_kappa1_upper*n_grad_v_upper_2, 0]
    ])
    # now we iterate the lower cell 
    for interval_idx in range(num_intervals):
        # left point of the interval:
        lp = interval_idx/num_intervals 
        left_idx = interval_idx+num_points_upper_cell
        lp_2d = list_p[left_idx]
        # right point of the interval:
        rp = (interval_idx+1)/num_intervals
        right_idx = (interval_idx+1)+num_points_upper_cell
        rp_2d = list_p[right_idx]
        # middle point of the small triangle
        middle_idx = interval_idx+num_points_upper_cell+num_points_lower_edge
        # fill in the integrals
        phi_0_upper = lambda s: (1-s)*(1-lp) + s*(1-rp)
        phi_1_upper = lambda s: (1-s)*lp + s*rp
        phi_left_lower = lambda s: 1-s
        phi_right_lower = lambda s: s 
        param = lambda s: (1-s)*lp_2d+s*rp_2d
        # these are negative, since contributions from different macro cells:
        int_phi_left_kappa1 = edge_length_small*gauss_quadrature_interval_3(lambda s: phi_left_lower(s)*kappa1(param(s)))
        int_phi_right_kappa1 = edge_length_small*gauss_quadrature_interval_3(lambda s: phi_right_lower(s)*kappa1(param(s)))
        int_phi_0_kappa2 = edge_length_small*gauss_quadrature_interval_3(lambda s: phi_0_upper(s)*kappa2(param(s)))
        int_phi_1_kappa2 = edge_length_small*gauss_quadrature_interval_3(lambda s: phi_1_upper(s)*kappa2(param(s)))
        # 0th row: test function grad(phi_0)
        K_loc[0, left_idx] += -int_phi_left_kappa1*n_grad_v_upper_0
        K_loc[0, right_idx] += -int_phi_right_kappa1*n_grad_v_upper_0
        # 1st row: test function grad(phi_1)
        K_loc[1, left_idx] += -int_phi_left_kappa1*n_grad_v_upper_1
        K_loc[1, right_idx] += -int_phi_right_kappa1*n_grad_v_upper_1
        # 3rd row: test function grad(phi_2)
        K_loc[2, left_idx] += -int_phi_left_kappa1*n_grad_v_upper_2
        K_loc[2, right_idx] += -int_phi_right_kappa1*n_grad_v_upper_2
        # (left_idx)th row: test function grad(phi_left)
        K_loc[left_idx, 0] += -int_phi_0_kappa2*n_grad_v_lower_0
        K_loc[left_idx, 1] += -int_phi_1_kappa2*n_grad_v_lower_0
        # (right_idx)th row: test function grad(phi_right)
        K_loc[right_idx, 0] += -int_phi_0_kappa2*n_grad_v_lower_1
        K_loc[right_idx, 1] += -int_phi_1_kappa2*n_grad_v_lower_1
        # (middle_idx)th row: test function grad(phi_middle)
        K_loc[middle_idx, 0] += -int_phi_0_kappa2*n_grad_v_lower_2
        K_loc[middle_idx, 1] += -int_phi_1_kappa2*n_grad_v_lower_2
        # these are positive, since contributions from the same macro cell:
        int_phi_left_kappa2 = edge_length_small*gauss_quadrature_interval_3(lambda s: phi_left_lower(s)*kappa2(param(s)))
        int_phi_right_kappa2 = edge_length_small*gauss_quadrature_interval_3(lambda s: phi_right_lower(s)*kappa2(param(s)))
        # (left_idx)th row: test function grad(phi_left)
        K_loc[left_idx, left_idx] += +int_phi_left_kappa2*n_grad_v_lower_0
        K_loc[left_idx, right_idx] += +int_phi_right_kappa2*n_grad_v_lower_0
        # (right_idx)th row: test function grad(phi_right)
        K_loc[right_idx, left_idx] += +int_phi_left_kappa2*n_grad_v_lower_1
        K_loc[right_idx, right_idx] += +int_phi_right_kappa2*n_grad_v_lower_1
        # (middle_idx)th row: test function grad(phi_middle)
        K_loc[middle_idx, left_idx] += +int_phi_left_kappa2*n_grad_v_lower_2
        K_loc[middle_idx, right_idx] += +int_phi_right_kappa2*n_grad_v_lower_2
    return K_loc


def extend(matrix):
    """
    Adds empty rows for missing dofs to make sure that a matrix assembled with
        assemble_local_jump_interior_dangling
    has the same shape as a matrix assembled with
        assemble_local_jump_avg_interior_dangling
    such that the matrices can be added.
    """
    num_edge_dofs = matrix.shape[0]-2
    num_additional_dofs = (num_edge_dofs-1)+1
    size = matrix.shape[0]+num_additional_dofs
    extension = np.zeros((size, size))
    extension[0:2,0:2] = matrix[0:2, 0:2]
    extension[0:2,3:3+num_edge_dofs] = matrix[0:2, 2:2+num_edge_dofs]
    extension[3:3+num_edge_dofs, 0:2] = matrix[2:2+num_edge_dofs, 0:2]
    extension[3:3+num_edge_dofs, 3:3+num_edge_dofs] = matrix[2:2+num_edge_dofs, 2:2+num_edge_dofs]
    return extension


def calculate_h(list_p):
    list_p = [np.array(p) for p in list_p]
    h1 = cell_diameter(list_p[0], list_p[1], list_p[2])
    h2 = cell_diameter(list_p[3], list_p[4], list_p[5])
    return max(h1, h2) 


def calculate_weight_exact(list_p, kappa_min_1, kappa_max_1, kappa_min_2, kappa_max_2):
    """
    Exact calculation of the weight by eq 2.38 in Riviere.
    """
    h = calculate_h(list_p)
    cottheta_1 = calculate_cot_theta(list_p[0], list_p[1], list_p[2])
    num_points_lower_edge = int((len(list_p)-3-1)/2+1)
    cottheta_2 = calculate_cot_theta(list_p[3], list_p[4], list_p[3+num_points_lower_edge])
    # cottheta_1 = 1 
    # cottheta_2 = 1 
    p = 1   # polynomial degree. here P1
    return p*(p+1)*3/2*(kappa_max_1**2/kappa_min_1*cottheta_1+kappa_max_2**2/kappa_min_2*cottheta_2)/h




def assemble_local_boundary_integrals_interior_dangling(
    list_p,
    kappa1, kappa2, 
    kappa_min1, kappa_max1, kappa_min2, kappa_max2,
    sigma=None
):
    """
    Assembles the terms
        - inner(jump(u,n), avg(kappa(x)*grad(v)))*dS - inner(avg(kappa(x)*grad(u)), jump(v,n))*dS
        + p*(p+1)/avg(h)*inner(jump(u,n), jump(v,n))*dS
    over the interior boundaries, where p=1 is the polynomial degree.
    The same preconditions as in assemble_local_jump_avg_interior_dangling are needed.
    kappa_min is a lower bound for kappa, and kappa_max an upper bound
    """
    num_points_upper_cell = 3
    num_points_lower_cell = len(list_p) - num_points_upper_cell
    num_points_lower_edge = int((num_points_lower_cell-1)/2+1)
    list_p_edge = list_p[:2] + list_p[3:3+num_points_lower_edge]
    K1 = assemble_local_jump_interior_dangling(list_p_edge)
    K2 = assemble_local_jump_avg_interior_dangling(list_p, kappa1, kappa2)
    if sigma is not None:
        h = calculate_h(list_p)
        # h = np.linalg.norm(list_p[0] - list_p[1])
        sigma = sigma/h
    else:
        sigma = calculate_weight_exact(list_p, kappa_min1, kappa_max1, kappa_min2, kappa_max2)
    return extend(sigma*K1)-K2-K2.transpose() 
