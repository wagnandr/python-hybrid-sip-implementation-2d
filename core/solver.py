from typing import List
from collections import namedtuple
import numpy as np
from petsc4py import PETSc

from core.macrocell import MacroCell
from core.mesh_topology import MeshTopology
from core import assemble_macrocells as amc
from core.section import section
from core.simple_vtk_ascii_writer import (
    SimpleVTKAsciiWriter,
    add_values_to_writer,
)


class RunMode:
    p1 = 'p1'
    dg = 'dg'
    dg_continuous_corners = 'dg-continuous-corners'


def initialize_macro_cells(mesh_topology: MeshTopology) -> List[MacroCell]:
    list_macrocells = []
    for cell_idx in range(mesh_topology.num_cells):
        N = mesh_topology.cell_to_refinement_level[cell_idx]
        v0, v1, v2, p0, p1, p2 = mesh_topology.get_points_in_ccw_orientation(cell_idx)
        macrocell = MacroCell(N, v0, v1, v2, p0, p1, p2)
        list_macrocells.append(macrocell)
    return list_macrocells


def initialize_continuous_dofs(mesh_topology: MeshTopology, macro_cells: List[MacroCell], just_corners: bool) -> int:
    next_dof_idx = 0
    # initialize all dofs to -1 per default which is an invalid idea
    for cell_idx in range(mesh_topology.num_cells):
        macro_cell = macro_cells[cell_idx]
        dof_map = -np.ones(macro_cell.num_dofs, dtype=np.int)
        macro_cell.set_dof_map(dof_map)
    # initialize macro vertex dofs
    for cell_idx in range(mesh_topology.num_cells):
        macro_cell = macro_cells[cell_idx]
        dof_map = macro_cell.vertex_to_dof_map
        dof_map[0] = macro_cell.v0
        dof_map[macro_cell.N] = macro_cell.v1
        dof_map[macro_cell.num_dofs-1] = macro_cell.v2
    next_dof_idx += mesh_topology.num_vertices
    # initialize shared macro edge dofs, if we want continuous edges
    if not just_corners:
        for cell_idx in range(mesh_topology.num_cells):
            macro_cell = macro_cells[cell_idx]
            dof_map = macro_cell.vertex_to_dof_map
            edge_indices = mesh_topology.cell_to_edge[cell_idx]
            for edge_idx in edge_indices:
                v_idx_1, v_idx_2 = mesh_topology.edge_to_vertex[edge_idx]
                vertices = np.array(macro_cell.get_vertices_boundary(v_idx_1, v_idx_2)[1:-1], dtype=np.int)
                # if it is an external boundary we can initialize it
                if mesh_topology.is_exterior_boundary(edge_idx):
                    num_dofs_to_assign = len(vertices)
                    if num_dofs_to_assign > 0:
                        dof_map[vertices] = np.arange(next_dof_idx, next_dof_idx+num_dofs_to_assign)
                        next_dof_idx += num_dofs_to_assign
                # if it is an interior edge we have to consider the neighboring cell
                else:
                    neighbour_cell_idx = mesh_topology.get_neighbour_cell(cell_idx, edge_idx)
                    neighbour_macro_cell = macro_cells[neighbour_cell_idx]
                    # do we have to merge the dofs of our neighbour cell?
                    # if it has a smaller index, it was already initialized and we have to copy the dof indices.
                    if neighbour_cell_idx < cell_idx:
                        neighbour_vertices = np.array(neighbour_macro_cell.get_vertices_boundary(v_idx_1, v_idx_2)[1:-1], dtype=np.int)
                        neighbour_dof_map = np.array(neighbour_macro_cell.vertex_to_dof_map)
                        assert len(vertices) == len(neighbour_vertices), 'only support equally spaced meshes'
                        # we assign the overlaping dofs
                        dof_map[vertices] = neighbour_dof_map[neighbour_vertices]
                    # if the dofs were not initialized before, we copy them
                    else:
                        num_dofs_to_assign = len(vertices)
                        if num_dofs_to_assign > 0 :
                            dof_map[vertices] = np.arange(next_dof_idx, next_dof_idx + num_dofs_to_assign)
                            next_dof_idx += num_dofs_to_assign
    # we set the remaining dofs inside the cells and convert the dof arrays to lists
    for cell_idx in range(mesh_topology.num_cells):
        macro_cell = macro_cells[cell_idx]
        dof_map = macro_cell.vertex_to_dof_map
        # we set the rest
        # and assign the dofmap
        num_dofs_to_assign = np.sum(dof_map == -1)
        if num_dofs_to_assign > 0:
            dof_map[dof_map == -1] = np.arange(next_dof_idx, next_dof_idx+num_dofs_to_assign)
        macro_cell.set_dof_map(dof_map.tolist())
        next_dof_idx += num_dofs_to_assign
    return next_dof_idx


def initialize_independent_dofs(mesh_topology: MeshTopology, macro_cells: List[MacroCell]) -> int:
    next_dof_idx = 0
    for cell_idx in range(mesh_topology.num_cells):
        macro_cell = macro_cells[cell_idx]
        next_dof_idx = macro_cell.init_dof_map(next_dof_idx)
    return next_dof_idx


def assemble_cells(K, f, macro_cells: List[MacroCell], list_kappa, boundary_value, rhs, verbose):
    for idx, (macro_cell, kappa) in enumerate(zip(macro_cells, list_kappa)):
        all_cells = macro_cell.get_all_cells()
        if verbose:
            section.info('assembling cells (cell {}) {}'.format(idx, all_cells))
        with section('assembling stiffness (cell {})'.format(idx)):
            amc.assemble_stiffness(K, all_cells, macro_cell.vertex_to_coord_map, macro_cell.vertex_to_dof_map, kappa)
        with section('assembling rhs (cell {})'.format(idx)):
            amc.assemble_rhs(f, all_cells, macro_cell.vertex_to_coord_map, macro_cell.vertex_to_dof_map, rhs)


def assemble_cells_mass(K, macro_cells: List[MacroCell]):
    for idx, macro_cell in enumerate(macro_cells):
        all_cells = macro_cell.get_all_cells()
        with section('assembling mass (cell {})'.format(idx)):
            amc.assemble_mass(K, all_cells, macro_cell.vertex_to_coord_map, macro_cell.vertex_to_dof_map)


def assemble_interior_integrals(K, mesh_topology: MeshTopology, macro_cells: List[MacroCell], list_kappa, kappa_min, kappa_max, verbose, sigma=None):
    for edge_idx in range(mesh_topology.num_edges):
        # we ignore all edges not in the interior
        if not mesh_topology.is_interior_boundary(edge_idx):
            continue
        cell_indices = mesh_topology.edge_to_cell[edge_idx]
        v_idx_0, v_idx_1 = mesh_topology.edge_to_vertex[edge_idx]
        cell_0 = macro_cells[cell_indices[0]]
        cell_1 = macro_cells[cell_indices[1]]
        # we make sure that cell_0 has less dofs than cell_1 by switching them if it is the case
        if cell_0.N > cell_1.N:
            cell_0, cell_1 = cell_1, cell_0
            cell_indices = list(reversed(cell_indices))
        # we make sure that the cells of cell_0 on the edge can be traversed in ccw orientation while retaining their order
        # if this is not possible, we switch the endpoint, thus traverse the edge in the other direction, which is fine too.
        if not cell_0.is_ccw_direction(v_idx_0, v_idx_1):
            v_idx_0, v_idx_1 = v_idx_1, v_idx_0
        cell_0_interior_boundary_cells = cell_0.get_boundary_retain_order(v_idx_0, v_idx_1)
        cell_1_interior_boundary_cells = cell_1.get_boundary_retain_order(v_idx_0, v_idx_1)
        if verbose:
            section.info('assembling over the following interior cells:')
            section.info('interior boundary cells {}: {}'.format(cell_indices[0], cell_0_interior_boundary_cells))
            section.info('interior boundary cells {}: {}'.format(cell_indices[1], cell_1_interior_boundary_cells))
        kappa_0 = list_kappa[cell_indices[0]]
        kappa_1 = list_kappa[cell_indices[1]]
        with section('assembling interior integrals (cells {} and {})'.format(cell_indices[0], cell_indices[1])):
            amc.assemble_boundary_integrals_interior_dangling(
                K, cell_0_interior_boundary_cells, cell_1_interior_boundary_cells, 
                cell_0.vertex_to_coord_map, 
                cell_1.vertex_to_coord_map, 
                cell_0.vertex_to_dof_map, 
                cell_1.vertex_to_dof_map,
                kappa_0, kappa_1,
                kappa_min, kappa_max,
                sigma)


def calculate_max_jump(u, mesh_topology: MeshTopology, macro_cells: List[MacroCell]):
    max_jump = 0
    for edge_idx in range(mesh_topology.num_edges):
        # we ignore all edges not in the interior
        if not mesh_topology.is_interior_boundary(edge_idx):
            continue
        cell_indices = mesh_topology.edge_to_cell[edge_idx]
        v_idx_0, v_idx_1 = mesh_topology.edge_to_vertex[edge_idx]
        cell_0 = macro_cells[cell_indices[0]]
        cell_1 = macro_cells[cell_indices[1]]
        # we make sure that cell_0 has the same number of dofs as cell_1 
        assert cell_0.N == cell_1.N
        cell_0_interior_boundary_cells = cell_0.get_boundary_retain_order(v_idx_0, v_idx_1)
        cell_1_interior_boundary_cells = cell_1.get_boundary_retain_order(v_idx_0, v_idx_1)
        for micro_cell_0, micro_cell_1 in zip(cell_0_interior_boundary_cells, cell_1_interior_boundary_cells):
            # we check if the first vertices are equal:
            assert np.allclose(cell_0.vertex_to_coord_map[micro_cell_0[0]], cell_1.vertex_to_coord_map[micro_cell_1[0]])
            assert np.allclose(cell_0.vertex_to_coord_map[micro_cell_0[1]], cell_1.vertex_to_coord_map[micro_cell_1[1]])
            for idx in range(0,2):
                idx0, idx1 = micro_cell_0[idx], micro_cell_1[idx]
                local_jump = abs(u[cell_0.vertex_to_dof_map[idx0]] - u[cell_1.vertex_to_dof_map[idx1]])
                max_jump = max(max_jump, local_jump)
    return max_jump


def multiply_with_inverse_diagonal(K): 
    diag_vector = K.getDiagonal()
    diag_vector[:] = 1. / diag_vector[:]
    iter_matrix = K.duplicate(copy=True)
    iter_matrix.diagonalScale(diag_vector)
    return iter_matrix


def apply_dirichlet_bcs(K, f, mesh_topology: MeshTopology, macro_cells: List[MacroCell], boundary_indicator, boundary_value):
    for macro_cell in macro_cells:
        amc.impose_dirichlet_bcs(K, f,
            macro_cell.vertex_to_coord_map, macro_cell.vertex_to_dof_map, 
            boundary_indicator, boundary_value)


def assemble_error(u, boundary_value, macro_cells: List[MacroCell]):
    error_squared = 0
    for macro_cell in macro_cells:
        all_cells = macro_cell.get_all_cells()
        error_squared += amc.assemble_error_norm_squared(all_cells, macro_cell.vertex_to_coord_map, macro_cell.vertex_to_dof_map, boundary_value, u)
    return np.sqrt(error_squared)


def save_vtk(u, macro_cells: List[MacroCell], filepath, name='u'):
    writer = SimpleVTKAsciiWriter()
    for macro_cell in macro_cells:
        all_cells = macro_cell.get_all_cells()
        add_values_to_writer(writer, all_cells, u, macro_cell.vertex_to_coord_map, macro_cell.vertex_to_dof_map)
    writer.write(filepath, name)


def interpolate(u_interp, macro_cells: List[MacroCell], boundary_value):
    for macro_cell in macro_cells:
        amc.interpolate(u_interp, macro_cell.vertex_to_coord_map, macro_cell.vertex_to_dof_map, boundary_value)


SolverResult = namedtuple('SolverResult', [
    'solution',     # the solution
    'size',         # the number of dofs
    'error_l2',     # the l2 error
    'macro_cells'   # a list of macro cells for preprocessing
])


def solve(
        mesh_topology: MeshTopology,
        run_mode,
        boundary_value,
        boundary_indicator,
        rhs,
        list_kappa,
        kappa_min,
        kappa_max,
        verbose
) -> SolverResult:
    macro_cells = initialize_macro_cells(mesh_topology)

    if run_mode == RunMode.dg:
        next_dof = initialize_independent_dofs(mesh_topology, macro_cells)
    elif run_mode == RunMode.p1:
        next_dof = initialize_continuous_dofs(mesh_topology, macro_cells, just_corners=False)
    elif run_mode == RunMode.dg_continuous_corners:
        next_dof = initialize_continuous_dofs(mesh_topology, macro_cells, just_corners=True)
    else:
        raise ValueError('invalid run mode') 

    # size of our problem
    size = next_dof

    K = PETSc.Mat().createAIJ(comm=PETSc.COMM_SELF, size=size, nnz=1000)
    K.setUp()
    u, f = K.createVecs()

    assemble_cells(K, f, macro_cells, list_kappa, boundary_value, rhs, verbose)
    if run_mode == RunMode.dg or run_mode == RunMode.dg_continuous_corners:
        assemble_interior_integrals(K, mesh_topology, macro_cells, list_kappa, kappa_min, kappa_max, verbose)

    # assemble
    K.assemble()
    f.assemble()

    with section('imposing dirichlet bcs'):
        apply_dirichlet_bcs(K, f, mesh_topology, macro_cells, boundary_indicator, boundary_value)

    # assemble dirichlet
    K.assemble()
    f.assemble()

    with section('solving'):
        ksp = PETSc.KSP()
        ksp.create(PETSc.COMM_WORLD)
        #ksp.setType(PETSc.Mat.SolverType.LUSOL)
        ksp.setType('preonly')
        ksp.getPC().setType('lu')
        ksp.setOperators(K)
        ksp.setFromOptions()
        ksp.solve(f, u)

    with section('assembling error'):
        error = assemble_error(u, boundary_value, macro_cells)
        section.info('error L2 = {}'.format(error))

    return SolverResult(
        solution=u,
        size=size,
        error_l2=error,
        macro_cells=macro_cells) 
