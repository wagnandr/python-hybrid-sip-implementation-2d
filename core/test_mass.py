
import sys
import slepc4py
slepc4py.init(sys.argv)
import petsc4py
from petsc4py import PETSc
petsc4py.init(sys.argv)
import numpy as np

from core.solver import (
    initialize_macro_cells,
    initialize_independent_dofs,
    assemble_cells_mass,
    interpolate
)
from core.rectangle_mesh import create_rectangle_mesh


def _boundary_indicator(p):
    return np.isclose(p[0], 0) or np.isclose(p[1], 0) or np.isclose(p[0], 1) or np.isclose(p[1], 1)


if __name__ == '__main__':
    n, m = 1, 4

    # we have NxN cells
    N = 2**m 

    # create the connectivity of our mesh
    mesh_topology = create_rectangle_mesh(np.array([0, 0]), np.array([1, 1]), n, N)

    macro_cells = initialize_macro_cells(mesh_topology)
    next_dof = initialize_independent_dofs(mesh_topology, macro_cells)

    size = next_dof

    K = PETSc.Mat().createAIJ(comm=PETSc.COMM_SELF, size=size, nnz=1000)
    K.setUp()
    u, f = K.createVecs()

    assemble_cells_mass(K, macro_cells)

    K.assemble()

    boundary_value = lambda x: 2*x[0]+3-3*x[1]
    interpolate(u, macro_cells, boundary_value)

    K.mult(u, f)

    print('got {}, expected {}'.format(f.sum(), 5./2))
