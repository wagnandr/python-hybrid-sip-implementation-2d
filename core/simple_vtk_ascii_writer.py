"""
Simple writer to vtk based on the example in 
    'Simple visualizations of unstructured grids with VTKRoman' by Putanowicz, Magoules
"""

import os


class SimpleVTKAsciiWriter:
    def __init__(self):
        self._vertices = []
        self._cells = []
        self._values = []
    
    def add_cell(self, p0, p1, p2, v0, v1, v2):
        idx = len(self._vertices)
        self._vertices.append(p0)
        self._vertices.append(p1)
        self._vertices.append(p2)
        self._values.append(v0)
        self._values.append(v1)
        self._values.append(v2)
        self._cells.append((idx, idx+1, idx+2))

    def write(self, path, name='u'):
        if path.endswith('vtk'):
            self.writeVTK(path, name)
        elif path.endswith('vtu'):
            self.writeVTU(path, name)
        else:
            print('Unkown file format in {}! Aborting!'.format(path))
            exit()
    
    def writeVTK(self, path, name='u'):
        with open(path, 'w') as f:
            f.write('# vtk DataFile Version 3.0\n')
            f.write('2D scalar data\n')
            f.write('ASCII\n')
            f.write('\n')
            f.write('DATASET UNSTRUCTURED_GRID\n')
            f.write('POINTS {} float\n'.format(len(self._vertices)))
            for x, y in self._vertices:
                f.write('{} {} 0\n'.format(x, y))
            f.write('\n')
            f.write('CELLS {} {}\n'.format(len(self._cells), 4*len(self._cells)))
            for idx1, idx2, idx3 in self._cells:
                f.write('3 {} {} {}\n'.format(idx1, idx2, idx3))
            f.write('\n')
            f.write('CELL_TYPES {}\n'.format(len(self._cells)))
            for _ in self._cells:
                f.write('5\n')
            f.write('\n')
            f.write('POINT_DATA {}\n'.format(len(self._values)))
            f.write('SCALARS {} float 1\n'.format(name))
            f.write('LOOKUP_TABLE default\n')
            f.write(' '.join(['{}'.format(v) for v in self._values]))
            f.write('\n')

    def writeVTU(self, path, name='u'):
        with open(path, 'w') as f:
            f.write('<VTKFile type="UnstructuredGrid">\n')
            f.write('<UnstructuredGrid>\n')
            f.write('<Piece NumberOfPoints="{}" NumberOfCells="{}">\n'.format(len(self._vertices), len(self._cells)))

            f.write('<Points>\n')
            f.write('<DataArray type="Float64" NumberOfComponents="3" format="ascii">\n')
            for x, y in self._vertices:
                f.write('{} {} 0\n'.format(x, y)) # newline not needed
            f.write('</DataArray>\n')
            f.write('</Points>\n')

            f.write('<Cells>\n')
            f.write('<DataArray type="Int32" Name="connectivity" format="ascii">\n')
            for idx1, idx2, idx3 in self._cells:
                f.write('{} {} {}\n'.format(idx1, idx2, idx3)) # newline not needed
            f.write('</DataArray>\n')
            f.write('<DataArray type="Int32" Name="offsets" format="ascii">\n')
            for idx in range(len(self._cells)):
                f.write('{}\n'.format(3*(idx+1))) # newline not needed
            f.write('</DataArray>\n')
            f.write('<DataArray type="Int32" Name="types" format="ascii">\n')
            for idx1, idx2, idx3 in self._cells:
                f.write('5\n') # newline not needed
            f.write('</DataArray>\n')
            f.write('</Cells>\n')

            f.write('<PointData Scalars="{}">\n'.format(name))
            f.write('<DataArray type="Float64" Name="{}" NumberOfComponents="1" format="ascii">\n'.format(name))
            f.write(' '.join(['{}'.format(v) for v in self._values]) + '\n')
            f.write('</DataArray>\n')
            f.write('</PointData>\n')

            f.write('</Piece>\n')
            f.write('</UnstructuredGrid>\n')
            f.write('</VTKFile>\n')


class SimplePVDWriter:
    def __init__(self):
        self._paths = []
        self._timesteps = []
    
    def add(self, path, timestep):
        self._paths.append(os.path.basename(path))
        self._timesteps.append(timestep)
    
    def write(self, path):
        with open(path, 'w') as f:
            f.write('<VTKFile type="Collection" version="0.1" byte_order="LittleEndian">\n')
            f.write('<Collection>\n')
            for p, t in zip(self._paths, self._timesteps):
                f.write('<DataSet timestep="{}" file="{}"/>\n'.format(t, p))
            f.write('</Collection>\n')
            f.write('</VTKFile>\n')


def add_values_to_writer(writer: SimpleVTKAsciiWriter, cells, values, vertex_to_coord_map, vertex_to_dof_map):
    for cell in cells:
        p = [vertex_to_coord_map[v_idx] for v_idx in cell]
        v = [values[vertex_to_dof_map[v_idx]] for v_idx in cell]
        writer.add_cell(p[0], p[1], p[2], v[0], v[1], v[2])


def _demo():
    writer = SimpleVTKAsciiWriter()
    writer.add_cell((0, 0), (1, 0), (1, 1), 10., 11., 12.)
    writer.add_cell((0, 0), (0, 1), (1, 1), 13., 14., 15.)
    writer.write('results/test_vtu.vtu')
    writer.write('results/test_vtk.vtk')


if __name__ == '__main__':
    _demo()
