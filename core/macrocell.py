"""
Manages dofs inside a macro cell
"""

import numpy as np

from core.assemble_local import create_affine_trafo


def calculate_offset(row_size, row):
    return int((row_size+1)*row_size/2 - (row_size+1-row)*(row_size-row)/2)


def get_local_vertex_index(N, row, col):
    assert 0 <= row
    assert 0 <= col
    assert row+col < N+1
    return calculate_offset(N+1, row) + col


class MacroCell:
    def __init__(self, N, v0, v1, v2, p0, p1, p2):
        self.N = N
        self.v0 = v0
        self.v1 = v1
        self.v2 = v2
        self.p0 = p0
        self.p1 = p1
        self.p2 = p2
        assert np.linalg.det(create_affine_trafo(p0, p1, p2)[0]) > 0, "only ccw orientations accepted."
        h_direction = (p1 - p0)/N
        v_direction = (p2 - p0)/N
        self.vertex_to_coord_map = []       # FIXME: Use numpy array here?
        self.vertex_to_dof_map = None       # FIXME: Use numpy array here?
        for row in range(N+1):
            for col in range(N+1-row):
                p = p0 + col*h_direction + row*v_direction
                self.vertex_to_coord_map.append(p)
    
    @property
    def num_dofs(self):
        return int((self.N+2)*(self.N+1)/2)

    def init_dof_map(self, start):
        assert self.num_dofs == len(self.vertex_to_coord_map)
        self.vertex_to_dof_map = list(range(start, start+self.num_dofs))
        return self.vertex_to_dof_map[-1]+1
    
    def set_dof_map(self, dof_map):
        self.vertex_to_dof_map = dof_map
    
    def get_all_cells(self):
        cells = []
        for row in range(self.N):
            for col in range(self.N-row):
                cell = (get_local_vertex_index(self.N, row, col), 
                        get_local_vertex_index(self.N, row, col+1), 
                        get_local_vertex_index(self.N, row+1, col))
                cells.append(cell)
                if col >= self.N-row-1:
                    continue
                cell = (get_local_vertex_index(self.N, row, col+1), 
                        get_local_vertex_index(self.N, row+1, col+1), 
                        get_local_vertex_index(self.N, row+1, col))
                cells.append(cell)
        return cells
    
    def get_boundary_ccw(self, v0_idx, v1_idx):
        if v0_idx == self.v0 and v1_idx == self.v1:
            return self.get_boundary_01_ccw()
        if v0_idx == self.v1 and v1_idx == self.v0:
            return self.get_boundary_10_ccw()
        if v0_idx == self.v0 and v1_idx == self.v2:
            return self.get_boundary_02_ccw()
        if v0_idx == self.v2 and v1_idx == self.v0:
            return self.get_boundary_20_ccw()
        if v0_idx == self.v1 and v1_idx == self.v2:
            return self.get_boundary_12_ccw()
        if v0_idx == self.v2 and v1_idx == self.v1:
            return self.get_boundary_21_ccw()
        raise ValueError('vertices not part of the macro cell')

    def get_boundary_cw(self, v0_idx, v1_idx):
        if v0_idx == self.v0 and v1_idx == self.v1:
            return self.get_boundary_01_cw()
        if v0_idx == self.v1 and v1_idx == self.v0:
            return self.get_boundary_10_cw()
        if v0_idx == self.v0 and v1_idx == self.v2:
            return self.get_boundary_02_cw()
        if v0_idx == self.v2 and v1_idx == self.v0:
            return self.get_boundary_20_cw()
        if v0_idx == self.v1 and v1_idx == self.v2:
            return self.get_boundary_12_cw()
        if v0_idx == self.v2 and v1_idx == self.v1:
            return self.get_boundary_21_cw()
        raise ValueError('vertices not part of the macro cell')

    def get_boundary_retain_order(self, v0_idx, v1_idx):
        """
        If we want to retain the order of vertices, which is useful for adaptivity, then the orientation is already
        fixed, and chosen in this function appropriately.
        """
        if v0_idx == self.v0 and v1_idx == self.v1:
            return self.get_boundary_01_ccw()
        if v0_idx == self.v1 and v1_idx == self.v0:
            return self.get_boundary_10_cw()
        if v0_idx == self.v0 and v1_idx == self.v2:
            return self.get_boundary_02_cw()
        if v0_idx == self.v2 and v1_idx == self.v0:
            return self.get_boundary_20_ccw()
        if v0_idx == self.v1 and v1_idx == self.v2:
            return self.get_boundary_12_ccw()
        if v0_idx == self.v2 and v1_idx == self.v1:
            return self.get_boundary_21_cw()
        raise ValueError('vertices not part of the macro cell')

    def is_ccw_direction(self, v0_idx, v1_idx):
        """
        Checks if the given direction can be iterated in ccw orientation, while retaining the order of edge vertices.
        """
        is_ccw = False
        is_ccw |= v0_idx == self.v0 and v1_idx == self.v1
        is_ccw |= v0_idx == self.v1 and v1_idx == self.v2
        is_ccw |= v0_idx == self.v2 and v1_idx == self.v0
        return is_ccw

    def get_boundary_01_ccw(self):
        return [
            (get_local_vertex_index(self.N, 0, col),
             get_local_vertex_index(self.N, 0, col+1),
             get_local_vertex_index(self.N, 1, col)) for col in range(self.N)]

    def get_boundary_10_ccw(self):
        return self.get_boundary_01_ccw()[::-1]

    def get_boundary_01_cw(self):
        return [
            (get_local_vertex_index(self.N, 0, col+1),
             get_local_vertex_index(self.N, 0, col),
             get_local_vertex_index(self.N, 1, col)) for col in range(self.N)]

    def get_boundary_10_cw(self):
        return self.get_boundary_01_cw()[::-1]
        
    def get_boundary_12_ccw(self):
        return [
            (get_local_vertex_index(self.N, row, self.N-row),
             get_local_vertex_index(self.N, row+1, self.N-row-1),
             get_local_vertex_index(self.N, row, self.N-row-1)) for row in range(self.N)]

    def get_boundary_21_ccw(self):
        return self.get_boundary_12_ccw()[::-1]

    def get_boundary_12_cw(self):
        return [
            (get_local_vertex_index(self.N, row+1, self.N-row-1),
             get_local_vertex_index(self.N, row, self.N-row),
             get_local_vertex_index(self.N, row, self.N-row-1)) for row in range(self.N)]

    def get_boundary_21_cw(self):
        return self.get_boundary_12_cw()[::-1]

    def get_boundary_02_ccw(self):
        return [
            (get_local_vertex_index(self.N, row+1, 0),
             get_local_vertex_index(self.N, row, 0),
             get_local_vertex_index(self.N, row, 1)) for row in range(self.N)]

    def get_boundary_20_ccw(self):
        return self.get_boundary_02_ccw()[::-1]
    
    def get_boundary_02_cw(self):
        return [
            (get_local_vertex_index(self.N, row, 0),
             get_local_vertex_index(self.N, row+1, 0),
             get_local_vertex_index(self.N, row, 1)) for row in range(self.N)]

    def get_boundary_20_cw(self):
        return self.get_boundary_02_cw()[::-1]
    
    def get_vertices_boundary_01(self):
        return [get_local_vertex_index(self.N, 0, col) for col in range(self.N+1)]

    def get_vertices_boundary_10(self):
        return self.get_vertices_boundary_01()[::-1]

    def get_vertices_boundary_12(self):
        return [get_local_vertex_index(self.N, row, self.N-row) for row in range(self.N+1)]

    def get_vertices_boundary_21(self):
        return self.get_vertices_boundary_12()[::-1]

    def get_vertices_boundary_02(self):
        return [get_local_vertex_index(self.N, row, 0) for row in range(self.N+1)]

    def get_vertices_boundary_20(self):
        return self.get_vertices_boundary_02()[::-1] 
    
    def get_vertices_boundary(self, v0_idx, v1_idx):
        if v0_idx == self.v0 and v1_idx == self.v1:
            return self.get_vertices_boundary_01()
        if v0_idx == self.v1 and v1_idx == self.v0:
            return self.get_vertices_boundary_10()
        if v0_idx == self.v0 and v1_idx == self.v2:
            return self.get_vertices_boundary_02()
        if v0_idx == self.v2 and v1_idx == self.v0:
            return self.get_vertices_boundary_20()
        if v0_idx == self.v1 and v1_idx == self.v2:
            return self.get_vertices_boundary_12()
        if v0_idx == self.v2 and v1_idx == self.v1:
            return self.get_vertices_boundary_21()
        raise ValueError('vertices not part of the macro cell')



def glue_dofs_01_with_21(macro_cell_1, macro_cell_2, next_dof):
    """
    Glues the dof on the edge 01 with the dofs on the edge 21, thus achieving continuity.
    We assume macro_cell_1 already has a valid dof map, but macrocell 2 has not.
    """
    assert macro_cell_1.vertex_to_dof_map is not None
    assert macro_cell_2.vertex_to_dof_map is None
    vertices_1 = np.array(macro_cell_1.get_vertices_boundary_01(), dtype=np.int)
    vertices_2 = np.array(macro_cell_1.get_vertices_boundary_21(), dtype=np.int)
    assert len(vertices_1) == len(vertices_2), 'only support equally spaces meshes'
    dof_map_1 = np.array(macro_cell_1.vertex_to_dof_map) 
    # all dofs are assigned -1, an invalid index per default
    dof_map_2 = -np.ones(macro_cell_2.num_dofs, dtype=np.int)     
    # we assign the right
    dof_map_2[vertices_2] = dof_map_1[vertices_1]
    num_other_dofs = macro_cell_2.num_dofs - len(vertices_2)
    # we set the rest
    dof_map_2[dof_map_2 == -1] = np.arange(next_dof, next_dof+num_other_dofs)
    # and assign the dofmap
    macro_cell_2.set_dof_map(dof_map_2.tolist())
    # return the next dof
    return next_dof+num_other_dofs
        

def plot_macro_cell(macro_cell):
    import matplotlib.pyplot as plt
    plt.axes()
    cells = macro_cell.get_all_cells()
    for cell in cells:
        p = [macro_cell.vertex_to_coord_map[v_idx] for v_idx in cell]
        line = plt.Line2D((p[0][0], p[1][0]), (p[0][1], p[1][1]), lw=1.5)
        plt.gca().add_line(line)
        line = plt.Line2D((p[0][0], p[2][0]), (p[0][1], p[2][1]), lw=1.5)
        plt.gca().add_line(line)
        line = plt.Line2D((p[1][0], p[2][0]), (p[1][1], p[2][1]), lw=1.5)
        plt.gca().add_line(line)
    for v_idx in range(len(macro_cell.vertex_to_coord_map)):
        p = macro_cell.vertex_to_coord_map[v_idx]
        plt.text(x=p[0], y=p[1], s='{}'.format(v_idx), horizontalalignment='center', verticalalignment='center')
    plt.axis('scaled')
    plt.show()


def _demo():
    import numpy as np
    p0 = np.array([0., 0.])
    p1 = np.array([1., 0.])
    p2 = np.array([0., 1.])
    macro_cell = MacroCell(2**1, 10, 11, 12, p0, p1, p2)
    print('all', macro_cell.get_all_cells())
    print('0-1 ccw', macro_cell.get_boundary_01_ccw())
    print('0-1 ccw', macro_cell.get_boundary_ccw(10, 11))
    print('0-1 cw', macro_cell.get_boundary_01_cw())
    print('0-1 cw', macro_cell.get_boundary_cw(10, 11))
    print('1-2 ccw', macro_cell.get_boundary_12_ccw())
    print('1-2 cw', macro_cell.get_boundary_12_cw())
    print('0-2 ccw', macro_cell.get_boundary_02_ccw())
    print('0-2 cw', macro_cell.get_boundary_02_cw())
    print('0-2 vertices', macro_cell.get_vertices_boundary(10, 12))
    print('2-0 vertices', macro_cell.get_vertices_boundary(12, 10))
    print('1-2 vertices', macro_cell.get_vertices_boundary(11, 12))
    print('2-1 vertices', macro_cell.get_vertices_boundary(12, 11))
    plot_macro_cell(macro_cell)


if __name__ == '__main__':
    _demo()