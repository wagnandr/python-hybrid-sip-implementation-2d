"""
Module for creating arbitrary rectangular macro meshes.
"""

import numpy as np

from core.mesh_topology import (
    MeshTopology,
    draw_mesh
)


def _num_horizontal_edges(n):
    return n*(n+1)


def _num_diagonal_edges(n):
    return n*n


def _num_vertical_edges(n):
    return (n+1)*n


def _get_horizontal_edge_index(n, row, col):
    return row*n + col 


def _get_diagonal_edge_index(n, row, col):
    return _num_horizontal_edges(n) + row*n + col


def _get_vertical_edge_index(n, row, col):
    return _num_horizontal_edges(n) + _num_diagonal_edges(n) + row*(n+1) + col


def create_rectangle_mesh(p0, p1, n, m):
    wx = (p1[0] - p0[0])/n
    wy = (p1[1] - p0[1])/n
    mesh_topology = MeshTopology()
    # add vertices
    for y in range(n+1):
        for x in range(n+1):
            mesh_topology.add_vertex(np.array([p0[0]+wx*x, p0[1]+wy*y]))
    # horizontal edges
    for row in range(n+1):
        for col in range(n):
            mesh_topology.add_edge(row*(n+1)+col, row*(n+1)+col+1)
    # diagonal edges
    for row in range(n):
        for col in range(n):
            mesh_topology.add_edge(row*(n+1)+col, (row+1)*(n+1)+col+1)
    # vertical edges
    for row in range(n):
        for col in range(n+1):
            mesh_topology.add_edge(row*(n+1)+col, (row+1)*(n+1)+col)
    # cells
    for row in range(n):
        for col in range(n):
            e0 = _get_vertical_edge_index(n, row, col)
            e1 = _get_horizontal_edge_index(n, row+1, col)
            e2 = _get_diagonal_edge_index(n, row, col)
            mesh_topology.add_cell(m, e0, e1, e2)
            e0 = _get_diagonal_edge_index(n, row, col)
            e1 = _get_vertical_edge_index(n, row, col+1)
            e2 = _get_horizontal_edge_index(n, row, col)
            mesh_topology.add_cell(m, e0, e1, e2)
    return mesh_topology


if __name__ == '__main__':
    n = 3
    mt = create_rectangle_mesh(np.array([-1, -1]), np.array([1, 1]), n, 1)
    draw_mesh(mt)
