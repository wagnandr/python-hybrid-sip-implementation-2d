"""
Small module for logging, which automatically adds indentation
and prints the time needed to execute the blocks.
"""

from timeit import default_timer as timer



class section:
    _section_indent = 0

    def __init__(self, message):
        self._message = message

    def __enter__(self):
        section._print_with_indent('Start: {}'.format(self._message))
        section._section_indent += 1
        self.start = timer()

    def __exit__(self, exc_type, exc_val, exc_tb):
        elapsed = timer() - self.start
        section._section_indent -= 1
        section._print_with_indent('End:   {}. [Took {:.2E}s].'.format(self._message, elapsed))
    
    @staticmethod
    def _print_with_indent(message):
        print(''.join(['-'] * section._section_indent) + message)

    @staticmethod
    def info(message):
        section._print_with_indent('INFO: '+message)



