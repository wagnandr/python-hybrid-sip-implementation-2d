"""
Class for storing the connectivity of a mesh.
"""

import numpy as np

from core.assemble_local import create_affine_trafo


class MeshTopology:
    def __init__(self):
        # vertex id is mapped to a numpy array
        self.vertex_to_coords = []
        # edge id is mapped to a vertex id tuple
        self.edge_to_vertex = []
        # cell id is mapped to a vertex id triple 
        self.cell_to_vertex = []
        # edge id is mapped to a cell id tuple
        self.edge_to_cell = []
        # cell id is mapped to an edge id list
        self.cell_to_edge = []
        # cell id to refinement level
        self.cell_to_refinement_level = []
        # vertex id to edge id list
        self.vertex_to_edge = {} 
        # vertex id to cell id list
        self.vertex_to_cell = {} 
    
    def add_vertex(self, coords):
        self.vertex_to_coords.append(coords)
        return len(self.vertex_to_coords)-1

    def add_edge(self, v_idx_1, v_idx_2):
        assert v_idx_1 != v_idx_2
        assert v_idx_1 < len(self.vertex_to_coords)
        assert v_idx_2 < len(self.vertex_to_coords)
        edge = tuple(sorted([v_idx_1, v_idx_2]))
        edge_idx = len(self.edge_to_vertex)
        self.edge_to_vertex.append(edge)
        self.edge_to_cell.append([])
        # add edge to vertices
        for v_idx in [v_idx_1, v_idx_2]:
            try:
                self.vertex_to_edge[v_idx]
            except:
                self.vertex_to_edge[v_idx] = []
            self.vertex_to_edge[v_idx] = list(set(self.vertex_to_edge[v_idx] + [edge_idx]))
        return edge_idx

    def add_cell(self, N, e_idx_1, e_idx_2, e_idx_3):
        # the edges should be different
        assert e_idx_1 != e_idx_2 != e_idx_3
        # the edge indices should be valid
        assert e_idx_1 < len(self.edge_to_vertex)
        assert e_idx_2 < len(self.edge_to_vertex)
        assert e_idx_3 < len(self.edge_to_vertex)
        # the edges should be of 3 independent points
        vertex_indices = tuple(sorted(list(set([v_idx for e_idx  in [e_idx_1, e_idx_2, e_idx_3] for v_idx in self.edge_to_vertex[e_idx]]))))
        assert len(vertex_indices) == 3 
        cell_idx = len(self.cell_to_vertex)
        self.cell_to_vertex.append(vertex_indices)
        for e_idx in [e_idx_1, e_idx_2, e_idx_3]:
            self.edge_to_cell[e_idx].append(cell_idx)
        self.cell_to_edge.append(tuple(sorted([e_idx_1, e_idx_2, e_idx_3])))
        self.cell_to_refinement_level.append(N)
        # add cell to vertices
        for v_idx in vertex_indices:
            try:
                self.vertex_to_cell[v_idx]
            except:
                self.vertex_to_cell[v_idx] = []
            self.vertex_to_cell[v_idx] = list(set(self.vertex_to_cell[v_idx] + [cell_idx]))
    
    def get_edge_idx(self, v_idx_1, v_idx_2):
        edge = tuple(sorted([v_idx_1, v_idx_2]))
        return self.edge_to_vertex.index(edge)

    def get_cell_idx(self, v_idx_1, v_idx_2, v_idx_3):
        cell = tuple(sorted([v_idx_1, v_idx_2, v_idx_3]))
        return self.cell_to_vertex.index(cell)
    
    def is_exterior_boundary(self, e_idx):
        return len(self.edge_to_cell[e_idx]) == 1

    def is_interior_boundary(self, e_idx):
        return len(self.edge_to_cell[e_idx]) == 2
    
    @property
    def num_cells(self):
        return len(self.cell_to_vertex)

    @property
    def num_edges(self):
        return len(self.edge_to_vertex)

    @property
    def num_vertices(self):
        return len(self.vertex_to_coords)
    
    def __str__(self):
        return '\n'.join([
            'vertex_to_coords {}'.format(self.vertex_to_coords),
            'edge_to_vertex {}'.format(self.edge_to_vertex),
            'cell_to_vertex {}'.format(self.cell_to_vertex),
            'edge_to_cell {}'.format(self.edge_to_cell),
            'cell_to_edge {}'.format(self.cell_to_edge),
            'cell_to_refinement_level {}'.format(self.cell_to_refinement_level)
        ])
    
    def get_points_in_ccw_orientation(self, cell_idx):
        vertex_indices = self.cell_to_vertex[cell_idx]
        assert len(vertex_indices) == 3
        p = [self.vertex_to_coords[v_idx] for v_idx in vertex_indices]
        B, _ = create_affine_trafo(p[0], p[1], p[2])
        if np.linalg.det(B) > 0:
            return vertex_indices[0], vertex_indices[1], vertex_indices[2], p[0], p[1], p[2]
        else:
            return vertex_indices[1], vertex_indices[0], vertex_indices[2], p[1], p[0], p[2]
    
    def get_neighbour_cell(self, cell_idx, edge_idx):
        cells = self.edge_to_cell[edge_idx]
        return cells[0] if cells[0] != cell_idx else cells[1]


def draw_mesh(mesh_topology: MeshTopology):
    import matplotlib.pyplot as plt
    plt.axes()
    # plot the edges
    for edge_idx in range(mesh_topology.num_edges):
        vertex_indices  = mesh_topology.edge_to_vertex[edge_idx]
        p = [mesh_topology.vertex_to_coords[v_idx] for v_idx in vertex_indices] 
        line = plt.Line2D((p[0][0], p[1][0]), (p[0][1], p[1][1]), lw=1.5)
        plt.gca().add_line(line)
    # plot the edge labels
    for edge_idx in range(mesh_topology.num_edges):
        vertex_indices  = mesh_topology.edge_to_vertex[edge_idx]
        p = [mesh_topology.vertex_to_coords[v_idx] for v_idx in vertex_indices] 
        x = (p[0][0]+p[1][0])/2
        y = (p[0][1]+p[1][1])/2
        plt.text(x=x, y=y, s='{}'.format(edge_idx), 
                 horizontalalignment='center', 
                 verticalalignment='center')
    # plot cell labels
    for cell_idx, vertex_indices in enumerate(mesh_topology.cell_to_vertex):
        p = [mesh_topology.vertex_to_coords[v_idx] for v_idx in vertex_indices]
        middle = (p[0] + p[1] + p[2])/3
        plt.text(x=middle[0], y=middle[1], s='{}'.format(cell_idx), 
                 horizontalalignment='center', 
                 verticalalignment='center')
    # plot vertex labels
    for v_idx in range(mesh_topology.num_vertices):
        p = mesh_topology.vertex_to_coords[v_idx]
        plt.text(x=p[0], y=p[1], s='{}'.format(v_idx), 
                 horizontalalignment='center', 
                 verticalalignment='center')
    plt.axis('scaled')
    plt.show()


def get_cell_midpoint(mesh_topology: MeshTopology, cell_idx):
    vertex_indices = mesh_topology.cell_to_vertex[cell_idx]
    midpoint = np.add.reduce([mesh_topology.vertex_to_coords[vertex_idx] for vertex_idx in vertex_indices])/3
    return midpoint


if __name__ == '__main__':
    N = 4
    mesh = MeshTopology()
    v1 = mesh.add_vertex(np.array([0,0]))
    v2 = mesh.add_vertex(np.array([1,0]))
    v3 = mesh.add_vertex(np.array([0,1]))
    v4 = mesh.add_vertex(np.array([1,1]))
    e12 = mesh.add_edge(v1, v2)
    e23 = mesh.add_edge(v2, v3)
    e13 = mesh.add_edge(v1, v3)
    c1 = mesh.add_cell(N, e12, e23, e13)
    e34 = mesh.add_edge(v3, v4)
    e24 = mesh.add_edge(v2, v4)
    c2 = mesh.add_cell(N, e24, e34, e23)
    print(mesh)
    print(mesh.get_cell_idx(v1, v3, v2))
    draw_mesh(mesh)
