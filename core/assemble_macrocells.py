"""
Routines to assemble the matrix and rhs.
"""

from petsc4py import PETSc
import numpy as np

from core.assemble_local import (
    assemble_local_boundary_integrals_interior,
    assemble_local_stiffness,
    assemble_local_mass,
    assemble_local_mass_rhs,
    assemble_local_error_norm_squared)
from core.assemble_local_dangling import assemble_local_boundary_integrals_interior_dangling


def assemble_stiffness(K, cells, vertex_to_coord_map, vertex_to_dof_map, kappa):
    for cell in cells:
        p = [vertex_to_coord_map[vidx] for vidx in cell]
        local_to_global_dof_map = [vertex_to_dof_map[vidx] for vidx in cell]
        K_loc = assemble_local_stiffness(p[0], p[1], p[2], kappa)
        for idx1_loc in range(3):
            for idx2_loc in range(3):
                idx1_glob = local_to_global_dof_map[idx1_loc]
                idx2_glob = local_to_global_dof_map[idx2_loc]
                K.setValue(idx1_glob, idx2_glob, K_loc[idx1_loc, idx2_loc], addv=PETSc.InsertMode.ADD_VALUES)


def assemble_mass(K, cells, vertex_to_coord_map, vertex_to_dof_map):
    for cell in cells:
        p = [vertex_to_coord_map[vidx] for vidx in cell]
        local_to_global_dof_map = [vertex_to_dof_map[vidx] for vidx in cell]
        K_loc = assemble_local_mass(p[0], p[1], p[2])
        for idx1_loc in range(3):
            for idx2_loc in range(3):
                idx1_glob = local_to_global_dof_map[idx1_loc]
                idx2_glob = local_to_global_dof_map[idx2_loc]
                K.setValue(idx1_glob, idx2_glob, K_loc[idx1_loc, idx2_loc], addv=PETSc.InsertMode.ADD_VALUES)


def assemble_boundary_integrals_interior(
    K,
    cells1, cells2,
    vertex_to_coord_map1, vertex_to_coord_map2,
    vertex_to_dof_map1, vertex_to_dof_map2, 
    kappa1, kappa2,
    kappa_min, kappa_max
):
    for cell1, cell2 in zip(cells1, cells2):
        p = [vertex_to_coord_map1[cell1[0]],
             vertex_to_coord_map1[cell1[1]],
             vertex_to_coord_map1[cell1[2]],
             vertex_to_coord_map2[cell2[2]]]
        # we check if the first vertices are equal:
        assert np.allclose(p[0], vertex_to_coord_map2[cell2[0]])
        assert np.allclose(p[1], vertex_to_coord_map2[cell2[1]])
        K_loc = assemble_local_boundary_integrals_interior(
            p[0], p[1], p[2], p[3], 
            kappa1, kappa2, 
            kappa_min, kappa_max, kappa_min, kappa_max)
        local_to_global_dof_map = [vertex_to_dof_map1[idx] for idx in cell1] 
        local_to_global_dof_map += [vertex_to_dof_map2[idx] for idx in cell2]
        for idx1_loc in range(6):
            for idx2_loc in range(6):
                idx1_glob = local_to_global_dof_map[idx1_loc]
                idx2_glob = local_to_global_dof_map[idx2_loc]
                K.setValue(idx1_glob, idx2_glob, K_loc[idx1_loc, idx2_loc], addv=PETSc.InsertMode.ADD_VALUES)


def assemble_rhs(f, cells, vertex_to_coord_map, vertex_to_dof_map, rhs):
    for cell in cells:
        p = [vertex_to_coord_map[v_idx] for v_idx in cell]
        local_to_global_dof_map = [vertex_to_dof_map[v_idx] for v_idx in cell]
        f_loc = assemble_local_mass_rhs(p[0], p[1], p[2], rhs)
        for idx_loc in range(3):
            idx_glob = local_to_global_dof_map[idx_loc]
            f.setValue(idx_glob, f_loc[idx_loc], addv=PETSc.InsertMode.ADD_ALL)


def assemble_error_norm_squared(cells, vertex_to_coord_map, vertex_to_dof_map, f, u):
    """
    Assembles int (u-f)^2*dx, where u is a vector representing a function in our HDG space 
    and f a function expression, which can be evaluated.
    """
    accum = 0
    for cell in cells:
        p = [vertex_to_coord_map[v_idx] for v_idx in cell]
        local_to_global_dof_map = [vertex_to_dof_map[v_idx] for v_idx in cell]
        u_local = [u[vertex_to_dof_map[v_idx]] for v_idx in cell]
        local_error = assemble_local_error_norm_squared(p[0], p[1], p[2], f, u_local[0], u_local[1], u_local[2])
        accum += local_error
    return accum


def impose_dirichlet_bcs(K, f, vertex_to_coord_map, vertex_to_dof_map, boundary_indicator, boundary_value):
    num_vertices = len(vertex_to_coord_map)
    dirichlet_dofs = []
    for v_idx in range(num_vertices):
        p = vertex_to_coord_map[v_idx]
        on_dirichlet = boundary_indicator(p)
        if on_dirichlet:
            dof = vertex_to_dof_map[v_idx]
            dirichlet_dofs.append(dof)
            f.setValue(dof, boundary_value(p), addv=PETSc.InsertMode.INSERT_VALUES)
    K.zeroRows(dirichlet_dofs, diag=1)


def interpolate(u, vertex_to_coord_map, vertex_to_dof_map, expression):
    num_vertices = len(vertex_to_coord_map)
    for v_idx in range(num_vertices):
        p = vertex_to_coord_map[v_idx]
        value = expression(p)
        dof = vertex_to_dof_map[v_idx]
        u[dof] = value


def assemble_boundary_integrals_interior_dangling(
        K,
        cells1, cells2,
        vertex_to_coord_map1, vertex_to_coord_map2,
        vertex_to_dof_map1, vertex_to_dof_map2,
        kappa1, kappa2,
        kappa_min, kappa_max,
        sigma
):
    """
    Careful! The nodes have to be ordered
    """
    assert len(cells1) <= len(cells2), "the ccw oriented cells have to be coarser"
    assert len(cells2) % len(cells1) == 0, "the number of cells on the finer edge, has to be a multiple of the number" \
                                           "of cells on the coarser edge"
    group_size = int(len(cells2)/len(cells1))
    cells2_grouped = _group_by(cells2, group_size)
    for cell1, cell2_list in zip(cells1, cells2_grouped):
        cell2_edge_nodes = [cell[0] for cell in cell2_list] + [cell2_list[-1][1]]
        cell2_interior_nodes = [cell[2] for cell in cell2_list]
        p = [vertex_to_coord_map1[idx] for idx in cell1]
        p += [vertex_to_coord_map2[idx] for idx in cell2_edge_nodes]
        p += [vertex_to_coord_map2[idx] for idx in cell2_interior_nodes]
        K_loc = assemble_local_boundary_integrals_interior_dangling(p, kappa1, kappa2, kappa_min, kappa_max, kappa_min, kappa_max, sigma)
        local_to_global_dof_map = [vertex_to_dof_map1[idx] for idx in cell1]
        local_to_global_dof_map += [vertex_to_dof_map2[idx] for idx in cell2_edge_nodes]
        local_to_global_dof_map += [vertex_to_dof_map2[idx] for idx in cell2_interior_nodes]
        size = len(cell1) + len(cell2_edge_nodes) + len(cell2_interior_nodes)
        for idx1_loc in range(size):
            for idx2_loc in range(size):
                idx1_glob = local_to_global_dof_map[idx1_loc]
                idx2_glob = local_to_global_dof_map[idx2_loc]
                K.setValue(idx1_glob, idx2_glob, K_loc[idx1_loc, idx2_loc], addv=PETSc.InsertMode.ADD_VALUES)


def _group_by(list_to_group, group_size):
    return list(zip(*(iter(list_to_group),) * group_size))
