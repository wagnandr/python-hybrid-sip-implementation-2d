from math import pi, pow, sin, cos, exp
import sys
import argparse
import petsc4py
import numpy as np
from matplotlib import pyplot as plt

from example_poisson import run as run_poisson
from example_poisson import LowerTriangleRefinementIndicator
from core.solver import RunMode


petsc4py.init(sys.argv)


PROBLEM_P3 = 'p3'
PROBLEM_SIN_SIMPLE = 'sin-simple'
PROBLEM_SIN_HARDER = 'sin-harder'
PROBLEM_LOGISTIC = 'logistic'
PROBLEM_LOGISTIC_JUMP = 'logistic-jump'


parser = argparse.ArgumentParser(description='Discover how the error behaves.')
parser.add_argument('-n', help='Size of the macro mesh.', type=int, default=1)
parser.add_argument('--start-m', help='With which m should we start', type=int, default=1)
parser.add_argument('--stop-m', help='With which m should we stop', type=int, default=5)
parser.add_argument('--refinement-factor', help='Which refinement should be applied to the lower triangle?.', type=int, default=1)
parser.add_argument('-rm', '--run-mode',
    help='Specifies which solver should be used, dg with and without continuous corners.', 
    choices=[RunMode.dg, RunMode.dg_continuous_corners],
    default=RunMode.dg)
parser.add_argument('-p', '--problem',
    help='Specifies which problem should be solved.', 
    choices=[PROBLEM_P3, PROBLEM_SIN_SIMPLE, PROBLEM_SIN_HARDER, PROBLEM_LOGISTIC, PROBLEM_LOGISTIC_JUMP],
    default=PROBLEM_P3)
args = parser.parse_args()

# polynomial degree 3
if args.problem == PROBLEM_P3:
    boundary_value = lambda x: 2*x[0]*x[0]*x[0]+3-3*x[1]*x[0]
    rhs = lambda x: -2*3*2*x[0]
    kappa = lambda x: 1
    kappa_min = kappa_max = 1
    name = 'problem_p3'
elif args.problem == PROBLEM_SIN_SIMPLE:
    boundary_value = lambda x: np.sin(2*pi*x[0])*np.cos(3*pi*x[1])
    rhs = lambda x: (4+9)*pi**2*np.sin(2*pi*x[0])*np.cos(3*pi*x[1])
    kappa = lambda x: 1
    kappa_min = kappa_max = 1
    name = 'problem_sin_simple'
elif args.problem == PROBLEM_SIN_HARDER:
    a = 2
    boundary_value = lambda x: sin(2*pi*a*x[0]*(pow(x[0], 2) + pow(1 - x[1], 2)))
    rhs = lambda x: 4*pow(pi, 2)*pow(a, 2)*pow(x[0], 2)*pow(2*x[1] - 2, 2)*sin(2*pi*a*x[0]*(pow(x[0], 2) + pow(1 - x[1], 2))) - 16*pi*a*x[0]*cos(2*pi*a*x[0]*(pow(x[0], 2) + pow(1 - x[1], 2))) + pow(4*pi*a*pow(x[0], 2) + 2*pi*a*(pow(x[0], 2) + pow(1 - x[1], 2)), 2)*sin(2*pi*a*x[0]*(pow(x[0], 2) + pow(1 - x[1], 2)))
    kappa = lambda x: 1
    kappa_min = kappa_max = 1
    name = 'problem_sin_harder'
elif args.problem == PROBLEM_LOGISTIC:
    # Logistic function
    a = 1e2
    boundary_value = lambda x: 1.0/(1 + exp(-a*(x[0] - x[1])))
    rhs = lambda x: 2*pow(a, 2)*exp(-a*(x[0] - x[1]))/pow(1 + exp(-a*(x[0] - x[1])), 2) - 4*pow(a, 2)*exp(-2*a*(x[0] - x[1]))/pow(1 + exp(-a*(x[0] - x[1])), 3)
    kappa = lambda x: 1
    kappa_min = kappa_max = 1
    name = 'problem_logistic'
else:
    a = 3e2
    boundary_value = lambda x: sin(2*pi*x[0])*cos(5*pi*x[1])
    kappa = lambda x: 1 + 0.5/(1 + exp(-a*(x[0] - x[1])))
    kappa_min = 0.5
    kappa_max = 1.5 
    rhs = lambda x: -2.5*pi*a*exp(-a*(x[0] - x[1]))*sin(2*pi*x[0])*sin(5*pi*x[1])/pow(1 + exp(-a*(x[0] - x[1])), 2) - 1.0*pi*a*exp(-a*(x[0] - x[1]))*cos(2*pi*x[0])*cos(5*pi*x[1])/pow(1 + exp(-a*(x[0] - x[1])), 2) + 29*pow(pi, 2)*(1 + 0.5/(1 + exp(-a*(x[0] - x[1]))))*sin(2*pi*x[0])*cos(5*pi*x[1])
    name = 'problem_logistic_jump'

error_list_strong = []
m_list = []
for m in range(args.start_m, args.stop_m+1):
    print('--------------------')
    error, size = run_poisson(args.n, m,
        plot_grids=False, 
        verbose=False, 
        run_mode=args.run_mode, 
        boundary_value=boundary_value, 
        rhs=rhs, 
        kappa=kappa, 
        kappa_min=kappa_min,
        kappa_max=kappa_max,
        refinement_indicator=LowerTriangleRefinementIndicator(args.refinement_factor),
        name=name+'_strong')
    error_list_strong.append(error)
    m_list.append(size)

plt.loglog(m_list, error_list_strong[0]*m_list[0]/np.array(m_list), ':', label='slope 1')
plt.loglog(m_list, error_list_strong,'o-', label='L2 error HDG (strong Dirichlet-BC)')
plt.xlabel('# dofs')
plt.ylabel('error')
plt.legend()
plt.grid(True)
plt.show()
