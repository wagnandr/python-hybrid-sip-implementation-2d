import sys
import os
import argparse
import slepc4py
import petsc4py
import numpy as np
from matplotlib import pyplot as plt

from core.utils import set_width_in_percent
from example_eigenvalues import EigenvalueCalculator


slepc4py.init(sys.argv)
petsc4py.init(sys.argv)


set_width_in_percent(0.5, 2, False)


def _fenics_eigenvalue(n, m, eigenvalue_indices):
    import dolfin as df
    mesh = df.RectangleMesh(df.Point(0, 0), df.Point(1, 1), n*(2**m), n*(2**m))
    V = df.FunctionSpace(mesh, "Lagrange", 1)

    u = df.TrialFunction(V)
    v = df.TestFunction(V)
    a = df.dot(df.grad(u), df.grad(v))*df.dx
    m = u*v*df.dx

    A = df.PETScMatrix()
    df.assemble(a, tensor=A)

    M = df.PETScMatrix()
    df.assemble(m, tensor=M)

    eigensolver = df.SLEPcEigenSolver(A, M)

    eigensolver.solve()

    size = len(V.dofmap().dofs())

    eigenvalues = [eigensolver.get_eigenpair(idx)[0] for idx in eigenvalue_indices]

    return eigenvalues, size


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plots the convergence of the conforming and hybrid dg method.')
    parser.add_argument('-n', help='Use a nxn macro grid.', type=int, default=1)
    parser.add_argument('-m', help='Use triangle cells with 2^m vertices on each side.', type=int, default=2)
    parser.add_argument('--eigenvalue-indices', help='Which eigenvalue should we compare?', nargs='+', type=int,
                        default=[1, 4])
    parser.add_argument('--sigmas', help='List of penalty parameters.', nargs='+', type=float,
                        default=[6, 60, 600, 6000, 60000, 600000])
    args = parser.parse_args()

    list_errors = []

    list_lambda_cg, size_cg = _fenics_eigenvalue(args.n, args.m, args.eigenvalue_indices)

    list_lambda_dg = []

    max_eigenvalue_idx = max(args.eigenvalue_indices)

    for sigma in args.sigmas:
        calc_dg = EigenvalueCalculator(args.n, args.m, sigma, use_mass=True, impose_continuity=False, inverse_diagonal=False)
        calc_dg.setup()
        offset = calc_dg.size - size_cg
        calc_dg.solve(offset+max_eigenvalue_idx+1)
        lambda_dg = [calc_dg.get_eigenvalue(offset+eigenvalue_idx) for eigenvalue_idx in args.eigenvalue_indices]
        list_lambda_dg.append(lambda_dg)

    for e_idx, eigenvalue_idx in enumerate(args.eigenvalue_indices):
        list_lambda_dg_ev = [list_lambda_dg[sigma_idx][e_idx] for sigma_idx in range(len(args.sigmas))]
        error = list(list_lambda_cg[e_idx] - np.array(list_lambda_dg_ev))
        plt.loglog(args.sigmas, error, '-o', label='$'+ '{}'.format(size_cg - eigenvalue_idx) +'^{th}$ eigenvalue')

        print('--------')
        print('size cg:', size_cg)
        print('eigenvalue: ', size_cg - eigenvalue_idx)
        print('lambda_cg', list_lambda_cg[e_idx])
        print('lambda_dg', list_lambda_dg_ev)
        print('error', error)

    list_lambda_dg_ev = [list_lambda_dg[sigma_idx][0] for sigma_idx in range(len(args.sigmas))]
    error = list(list_lambda_cg[0] - np.array(list_lambda_dg_ev))

    plt.loglog(np.array(args.sigmas)[1:-1], 1/np.array(args.sigmas)[1:-1]*args.sigmas[0]*error[0], ':', label='linear', color='gray')
    plt.ylabel('$\\lambda_k - \\lambda^{SIPG}_{\\sigma, k}$')
    plt.xlabel('penalty parameter $\\sigma$')
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(os.path.join('results', 'eigenvalue-convergence.pgf'))
    plt.show()
