import sys
from math import sqrt
import os
import argparse
import slepc4py
import petsc4py
import numpy as np
from matplotlib import pyplot as plt

from core.utils import set_width_in_percent
from example_eigenvalues import EigenvalueCalculator


slepc4py.init(sys.argv)
petsc4py.init(sys.argv)


set_width_in_percent(0.75, 1, False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run the Hybrid DG test problem.')
    parser.add_argument('-n', help='Use a nxn macro grid.', type=int, default=1)
    parser.add_argument('--list-m', help='Use triangle cells with 2^m+1 vertices on each side.', nargs='+', type=int, default=[1, 2, 3])
    parser.add_argument('--sigma', help='penalty parameter', type=float, default=6)
    args = parser.parse_args()

    list_lambda_dg_0 = []
    list_lambda_dg = []
    list_lambda_dg_sigma = []
    list_h_inv = []

    for m in args.list_m:
        calc_dg = EigenvalueCalculator(args.n, m, args.sigma, use_mass=True, impose_continuity=False, inverse_diagonal=False)
        calc_dg.setup()
        h_inv = (args.n*2**m)/sqrt(2)
        list_h_inv.append(h_inv)
        max_continuous_eigenvalue_idx = (args.n * 2**m + 1)**2
        max_eigenvalue_idx = calc_dg.size - max_continuous_eigenvalue_idx
        print('{}x{}={}'.format(h_inv+1, h_inv+1, max_continuous_eigenvalue_idx))
        calc_dg.solve(max_eigenvalue_idx)
        eigenvalue_idx = max_eigenvalue_idx-1
        #eigenvalue_idx = 3 
        lambda_dg = calc_dg.get_eigenvalue(eigenvalue_idx)
        list_lambda_dg.append(lambda_dg)
        lambda_dg = calc_dg.get_eigenvalue(0)
        list_lambda_dg_0.append(lambda_dg) 
        # add
        # calc_dg = EigenvalueCalculator(args.n, m, 10000*args.sigma, use_mass=True, impose_continuity=False, inverse_diagonal=False)
        # calc_dg.setup()
        # calc_dg.solve(max_eigenvalue_idx)
        # lambda_dg = calc_dg.get_eigenvalue(eigenvalue_idx)
        # list_lambda_dg_sigma.append(lambda_dg)
    
    plt.loglog(list_h_inv, list_lambda_dg_0, '-o', label='largest spurious eigenvalue ')
    plt.loglog(list_h_inv, list_lambda_dg, '-o', label='smallest spurious eigenvalue')
    # plt.plot(list_h_inv, list_lambda_dg_sigma, label='lower bound')

    plt.loglog(np.array(list_h_inv)[1:-1], np.array(list_h_inv)[1:-1]**2*list_lambda_dg_0[0], ':', label='quadratic', color='gray')

    plt.ylabel('$\\lambda^{SIPG}_{\\sigma, k}$')
    plt.xlabel('$h^{-1}$')
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(os.path.join('results', 'eigenvalue-h-divergence.pgf'))
    plt.show()
