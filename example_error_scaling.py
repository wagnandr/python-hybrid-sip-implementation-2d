from math import pi, pow, sin, cos, exp
import sys
import argparse
import petsc4py
import numpy as np
from matplotlib import pyplot as plt

from example_poisson import run as run_poisson
from core.solver import RunMode


petsc4py.init(sys.argv)


def run_poisson_in_fenics(n, boundary_value, rhs):
    import dolfin as df

    N = 2**n
    mesh = df.RectangleMesh(df.Point(0, 0), df.Point(1, 1), N, N)

    class BV(df.UserExpression):
        def __init__(self, **kwargs):
            super().__init__(degree=3, **kwargs)

        def eval(self, values, x):
            values[0] = boundary_value(x)
        
        def value_shape(self):
            return ()

    class RHS(df.UserExpression):
        def __init__(self, **kwargs):
            super().__init__(degree=1, **kwargs)

        def eval(self, values, x):
            values[0] = rhs(x)

        def value_shape(self):
            return ()

    bv_exp = BV() 
    rhs_exp = RHS() 

    V = df.FunctionSpace(mesh, 'P', 1)
    u = df.TrialFunction(V)
    v = df.TestFunction(V)
    a = df.inner(df.grad(u), df.grad(v))*df.dx
    f = rhs_exp*v*df.dx 

    bc = df.DirichletBC(V, bv_exp, df.CompiledSubDomain('on_boundary'))

    u_sol = df.Function(V)

    df.solve(a ==f, u_sol, [bc])

    return df.errornorm(BV(), u_sol, 'L2')


PROBLEM_P3 = 'p3'
PROBLEM_SIN_SIMPLE = 'sin-simple'
PROBLEM_SIN_HARDER = 'sin-harder'
PROBLEM_LOGISTIC = 'logistic'
PROBLEM_LOGISTIC_JUMP = 'logistic-jump'


parser = argparse.ArgumentParser(description='Discover how the error behaves.')
parser.add_argument('--compare-to-fenics', help='We also compare to a P1-fenics-solver', action='store_true')
parser.add_argument('-n', help='Size of the macro mesh.', type=int, default=1)
parser.add_argument('--start-m', help='With which m should we start', type=int, default=1)
parser.add_argument('--stop-m', help='With which m should we stop', type=int, default=5)
parser.add_argument('-rm', '--run-mode', 
    help='Specifies which solver should be used, p1, hdg with strong DBC or hdg with weak DBC.', 
    choices=[RunMode.p1, RunMode.dg],
    default=RunMode.dg)
parser.add_argument('-p', '--problem', 
    help='Specifies which problem should be solved.', 
    choices=[PROBLEM_P3, PROBLEM_SIN_SIMPLE, PROBLEM_SIN_HARDER, PROBLEM_LOGISTIC, PROBLEM_LOGISTIC_JUMP],
    default=PROBLEM_P3)
args = parser.parse_args()

# polynomial degree 3
if args.problem == PROBLEM_P3:
    boundary_value = lambda x: 2*x[0]*x[0]*x[0]+3-3*x[1]*x[0]
    rhs = lambda x: -2*3*2*x[0]
    kappa = lambda x: 1
    kappa_min = kappa_max = 1
    name = 'problem_p3'
elif args.problem == PROBLEM_SIN_SIMPLE:
    boundary_value = lambda x: np.sin(2*pi*x[0])*np.cos(3*pi*x[1])
    rhs = lambda x: (4+9)*pi**2*np.sin(2*pi*x[0])*np.cos(3*pi*x[1])
    kappa = lambda x: 1
    kappa_min = kappa_max = 1
    name = 'problem_sin_simple'
elif args.problem == PROBLEM_SIN_HARDER:
    a = 2
    boundary_value = lambda x: sin(2*pi*a*x[0]*(pow(x[0], 2) + pow(1 - x[1], 2)))
    rhs = lambda x: 4*pow(pi, 2)*pow(a, 2)*pow(x[0], 2)*pow(2*x[1] - 2, 2)*sin(2*pi*a*x[0]*(pow(x[0], 2) + pow(1 - x[1], 2))) - 16*pi*a*x[0]*cos(2*pi*a*x[0]*(pow(x[0], 2) + pow(1 - x[1], 2))) + pow(4*pi*a*pow(x[0], 2) + 2*pi*a*(pow(x[0], 2) + pow(1 - x[1], 2)), 2)*sin(2*pi*a*x[0]*(pow(x[0], 2) + pow(1 - x[1], 2)))
    kappa = lambda x: 1
    kappa_min = kappa_max = 1
    name = 'problem_sin_harder'
elif args.problem == PROBLEM_LOGISTIC:
    # Logistic function
    a = 1e2
    boundary_value = lambda x: 1.0/(1 + exp(-a*(x[0] - x[1])))
    rhs = lambda x: 2*pow(a, 2)*exp(-a*(x[0] - x[1]))/pow(1 + exp(-a*(x[0] - x[1])), 2) - 4*pow(a, 2)*exp(-2*a*(x[0] - x[1]))/pow(1 + exp(-a*(x[0] - x[1])), 3)
    kappa = lambda x: 1
    kappa_min = kappa_max = 1
    name = 'problem_logistic'
else:
    a = 3e2
    boundary_value = lambda x: sin(2*pi*x[0])*cos(5*pi*x[1])
    kappa = lambda x: 1 + 0.5/(1 + exp(-a*(x[0] - x[1])))
    kappa_min = 0.5
    kappa_max = 1.5 
    rhs = lambda x: -2.5*pi*a*exp(-a*(x[0] - x[1]))*sin(2*pi*x[0])*sin(5*pi*x[1])/pow(1 + exp(-a*(x[0] - x[1])), 2) - 1.0*pi*a*exp(-a*(x[0] - x[1]))*cos(2*pi*x[0])*cos(5*pi*x[1])/pow(1 + exp(-a*(x[0] - x[1])), 2) + 29*pow(pi, 2)*(1 + 0.5/(1 + exp(-a*(x[0] - x[1]))))*sin(2*pi*x[0])*cos(5*pi*x[1])
    name = 'problem_logistic_jump'

error_list_strong = []
error_list_weak = []
error_list_fenics = []
error_list_p1 = []
m_list = []
for m in range(args.start_m, args.stop_m+1):
    print('--------------------')
    print('dg:')
    print('--------------------')
    error, _ = run_poisson(args.n, m,
        plot_grids=False, 
        verbose=False, 
        run_mode=RunMode.dg, 
        boundary_value=boundary_value, 
        rhs=rhs, 
        kappa=kappa, 
        kappa_min=kappa_min,
        kappa_max=kappa_max,
        refinement_indicator=lambda x: 1,
        name=name+'_strong')
    error_list_strong.append(error)
    print('--------------------')
    print('P1:                 ')
    print('--------------------')
    error, _ = run_poisson(args.n, m,
        plot_grids=False, 
        verbose=False, 
        run_mode=RunMode.p1, 
        boundary_value=boundary_value, 
        rhs=rhs, 
        kappa=kappa, 
        kappa_min=kappa_min,
        kappa_max=kappa_max,
        refinement_indicator=lambda x: 1,
        name=name+'_p1')
    error_list_p1.append(error)
    if args.compare_to_fenics:
        error = run_poisson_in_fenics(n, boundary_value, rhs)
        error_list_fenics.append(error)
    m_list.append(2**m)

plt.loglog(m_list, 1/np.array(m_list)**1, ':', label='slope 1')
plt.loglog(m_list, 1/np.array(m_list)**2, ':', label='slope 2')
# plt.loglog(n_list, 1/np.array(n_list)**(2.5), '--', label='slope 2.5')
plt.loglog(m_list, error_list_strong,'o-', label='L2 error HDG (strong Dirichlet-BC)')
plt.loglog(m_list, error_list_p1,'+:', label='L2 error P1')
if args.compare_to_fenics:
    plt.loglog(m_list, error_list_fenics,'x--', label='L2 error (P1-fenics)')
plt.xlabel('# grid points along one axis')
plt.ylabel('error')
plt.legend()
plt.grid(True)
plt.show()
