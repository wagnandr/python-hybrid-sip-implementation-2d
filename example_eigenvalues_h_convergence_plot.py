import sys
from math import pi
import argparse
import slepc4py
import petsc4py
import numpy as np
from matplotlib import pyplot as plt

from core.utils import set_width_in_percent
from example_eigenvalues import EigenvalueCalculator
from example_eigenvalues_convergence_plot import _fenics_eigenvalue


slepc4py.init(sys.argv)
petsc4py.init(sys.argv)


set_width_in_percent(0.75, 1, False)


def get_neumann_eigenvalue(k, l):
    return pi**2 * (k**2 + l**2)


def get_neumann_eigenvalues(number_of_eigenvalues):
    eigenvalues = []
    for k in range(number_of_eigenvalues):
        for l in range(k+1):
            val = get_neumann_eigenvalue(k, l)
            eigenvalues.append(val)
            if k != l:
                eigenvalues.append(val)
    return sorted(eigenvalues)[:number_of_eigenvalues]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run the Hybrid DG test problem.')
    parser.add_argument('-n', help='Use a nxn macro grid.', type=int, default=3)
    parser.add_argument('--list-m', help='Use triangle cells with 2^m+1 vertices on each side.', nargs='+', type=int,
                        default=[0, 1, 2, 3])
    parser.add_argument('--sigma', help='penalty parameter', type=float, default=6)
    parser.add_argument('--eigenvalues-to-plot', help='eigenvalues to plot', nargs='+', type=int, default=[2, 8])
    parser.add_argument('--use-dg', help='also show pure DG', action='store_true')
    args = parser.parse_args()

    list_h_inv = []
    list_lambda_hdg = []
    list_lambda_dg = []
    list_lambda_cg = []
    list_lambda_analytic = []

    for m in args.list_m:
        calc_dg = EigenvalueCalculator(args.n, m, args.sigma, use_mass=True, impose_continuity=False, inverse_diagonal=False)
        calc_dg.setup()
        h_inv = (args.n*2**m)
        list_h_inv.append(h_inv)
        max_continuous_eigenvalue_idx = (args.n * 2**m + 1)**2
        calc_dg.solve(calc_dg.size)
        list_lambda_hdg.append([calc_dg.get_eigenvalue(idx) for idx in range(calc_dg.size-1, calc_dg.size-max_continuous_eigenvalue_idx-1, -1)])
        list_lambda_cg.append(_fenics_eigenvalue(args.n, m, list(range(max_continuous_eigenvalue_idx-1, -1, -1)))[0])
        list_lambda_analytic.append(get_neumann_eigenvalues(max_continuous_eigenvalue_idx))
        if args.use_dg:
            calc_dg = EigenvalueCalculator(args.n*2**m, 0, args.sigma, use_mass=True, impose_continuity=False, inverse_diagonal=False)
            calc_dg.setup()
            calc_dg.solve(calc_dg.size)
            list_lambda_dg.append([calc_dg.get_eigenvalue(idx) for idx in range(calc_dg.size-1, calc_dg.size-max_continuous_eigenvalue_idx-1, -1)])

    for eigenvalue_idx in args.eigenvalues_to_plot:
        error_cg = [(lcg[eigenvalue_idx]-ldga[eigenvalue_idx])/ldga[eigenvalue_idx] for (lcg, ldga) in zip(list_lambda_cg, list_lambda_analytic)]
        #error_cg = [lcg[eigenvalue_idx] for (lcg, ldga) in zip(list_lambda_cg, list_lambda_analytic)]
        label = 'CG'
        if len(args.eigenvalues_to_plot) > 1:
            label = '{} ({})'.format(label, eigenvalue_idx)
        plt.loglog(list_h_inv, error_cg, '.-', label=label)

        error_hdg = [(ldg[eigenvalue_idx]-ldga[eigenvalue_idx])/ldga[eigenvalue_idx] for (ldg, ldga) in zip(list_lambda_hdg, list_lambda_analytic)]
        #error_hdg = [ldg[eigenvalue_idx] for (ldg, ldga) in zip(list_lambda_hdg, list_lambda_analytic)]
        label = 'hybrid SIPG'
        if len(args.eigenvalues_to_plot) > 1:
            label = '{} ({})'.format(label, eigenvalue_idx)
        plt.loglog(list_h_inv, error_hdg, '.-', label=label)

        if args.use_dg:
            error_dg = [(ldg[eigenvalue_idx]-ldga[eigenvalue_idx])/ldga[eigenvalue_idx] for (ldg, ldga) in zip(list_lambda_dg, list_lambda_analytic)]
            # error_dg = [ldg[eigenvalue_idx] for (ldg, ldga) in zip(list_lambda_dg, list_lambda_analytic)]
            label = 'SIPG'
            if len(args.eigenvalues_to_plot) > 1:
                label = '{} ({})'.format(label, eigenvalue_idx)
            plt.loglog(list_h_inv, error_dg, '.-', label=label)

        if eigenvalue_idx == max(args.eigenvalues_to_plot):
            plt.loglog(np.array(list_h_inv)[1:3], 10*(1/np.array(list_h_inv)[1:3]*list_h_inv[0]*error_cg[0])**2, ':', label='quadratic', color='gray')

    for dg, cg in zip(list_lambda_hdg, list_lambda_cg):
        print(len(dg), len(cg))

    plt.legend()
    plt.grid(True)
    plt.ylabel('$\\frac{\\lambda^{ana}_k - \\lambda_{k, h}}{\\lambda^{ana}_k}$')
    plt.xlabel('$h^{-1}$')
    plt.tight_layout()
    plt.show()
