"""
Plots the solution of the singular solution example in the Riviere book.
"""

import argparse
import numpy as np

from example_singularity import analytic_solution_fct
from core.solver import interpolate, initialize_macro_cells, initialize_continuous_dofs, save_vtk
from core.simple_vtk_ascii_writer import SimpleVTKAsciiWriter
from core.rectangle_mesh import create_rectangle_mesh


parser = argparse.ArgumentParser(description='Plot sinularity.')
parser.add_argument('-N', help='How fine?', type=int, default=1)
args = parser.parse_args()

mesh_topology = create_rectangle_mesh(np.array([-1, -1]), np.array([1, 1]), 2, args.N)

macro_cells = initialize_macro_cells(mesh_topology)
size = initialize_continuous_dofs(mesh_topology, macro_cells, just_corners=False)

u = np.zeros(size)

interpolate(u, macro_cells, analytic_solution_fct)
writer = SimpleVTKAsciiWriter()
filepath = 'results/singularity_fine_interpolated_{}.vtk'.format(args.N)
save_vtk(u, macro_cells, filepath)