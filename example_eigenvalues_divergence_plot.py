import sys
import os
import argparse
import slepc4py
import petsc4py
import numpy as np
from matplotlib import pyplot as plt

from core.utils import set_width_in_percent
from example_eigenvalues import EigenvalueCalculator


slepc4py.init(sys.argv)
petsc4py.init(sys.argv)


set_width_in_percent(0.5, 2, False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plots the divergence of the spurious eigenmodes of the hybrid dg method.')
    parser.add_argument('-n', help='Use a nxn macro grid.', type=int, default=3)
    parser.add_argument('-m', help='Use triangle cells with 2^m vertices on each side.', type=int, default=3)
    parser.add_argument('--eigenvalue-indices', help='Which eigenvalue should we compare?', nargs='+', type=int,
                        default=[0, 100, 184, 185])
    parser.add_argument('--sigmas', help='List of penalty parameters.', nargs='+', type=float,
                        default=[6, 60, 600, 6000, 60000, 600000])
    args = parser.parse_args()

    list_lambda_dg = []

    max_eigenvalue_idx = max(args.eigenvalue_indices)

    for sigma in args.sigmas:
        calc_dg = EigenvalueCalculator(args.n, args.m, sigma, use_mass=True, impose_continuity=False, inverse_diagonal=False)
        calc_dg.setup()
        calc_dg.solve(max_eigenvalue_idx+1)
        lambda_dg = [calc_dg.get_eigenvalue(eigenvalue_idx) for eigenvalue_idx in args.eigenvalue_indices]
        list_lambda_dg.append(lambda_dg)

    for e_idx, eigenvalue_idx in enumerate(args.eigenvalue_indices):
        list_lambda_dg_ev = [list_lambda_dg[sigma_idx][e_idx] for sigma_idx in range(len(args.sigmas))]
        plt.loglog(args.sigmas, list_lambda_dg_ev, '-o', label='$'+ '{}'.format(calc_dg.size - eigenvalue_idx) +'^{th}$ eigenvalue')

        print('--------')
        print('eigenvalue: ', calc_dg.size - eigenvalue_idx)
        print('lambda_dg', list_lambda_dg_ev)

    list_lambda_dg_max = max([list_lambda_dg[0][eidx] for eidx in range(len(args.eigenvalue_indices))])

    plt.loglog(np.array(args.sigmas)[1:-1], np.array(args.sigmas)[1:-1]*2*list_lambda_dg_max, ':', label='linear', color='gray')

    plt.ylabel('$\\lambda^{SIPG}_{\\sigma, k}$')
    plt.xlabel('penalty parameter $\\sigma$')
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(os.path.join('results', 'eigenvalue-divergence.pgf'))
    plt.show()
