import argparse
import numpy as np
import dolfin as df
from matplotlib import pyplot as plt

from core.assemble_local import (
    assemble_local_jump_interior,
    assemble_local_jump_avg_interior,
    assemble_local_minus_jump_avg_sym_interior,
    assemble_local_boundary_integrals_interior,
    assemble_local_mass_rhs
)
from core.assemble_local_dangling import (
    assemble_local_jump_interior_dangling,
    assemble_local_jump_avg_interior_dangling
)
    

parser = argparse.ArgumentParser(description='Compare the fenics entries with our own.')
parser.add_argument('-cj', '--compare-jump',
                    help='Compares the jump terms [[u]][[v]].', action='store_true')
parser.add_argument('-ca', '--compare-average',
                    help='Compares the jump term {{kappa(x)*grad(u)}}[[v]].', action='store_true')
parser.add_argument('-cs', '--compare-symmetric',
                    help='Compares the symmetric jump terms -{{kappa(x)*grad(u)}}[[v]]-[[u]]{{kappa(x)*grad(v)}}.',
                    action='store_true')
parser.add_argument('-cb', '--compare-boundary',
                    help='Compares all boundary parts, i.e. 2/h[[u]][[v]]-{{kappa(x)*grad(u)}}[[v]]-[[u]]{{kappa(x)*grad(v)}}.',
                    action='store_true')
parser.add_argument('-crm', '--compare-rhs-mass',
                    help='Compares the volume part of the rhs.', action='store_true')
parser.add_argument('-pm', '--plot-mesh',
                    help='Plots the mesh.', action='store_true')
parser.add_argument('-ll', '--lower-left',
                    help='coordinate of the lower left point.', nargs='+', type=float, default=[0., 0.])
parser.add_argument('-lr', '--lower-right',
                    help='coordinate of the lower right point.', nargs='+', type=float, default=[1., 0.])
parser.add_argument('-ul', '--upper-left',
                    help='coordinate of the upper left point.', nargs='+', type=float, default=[0., 1.])
parser.add_argument('-ur', '--upper-right',
                    help='coordinate of the upper right point.', nargs='+', type=float, default=[1., 1.])
args = parser.parse_args()

assert len(args.lower_left) == 2
assert len(args.lower_right) == 2
assert len(args.upper_left) == 2
assert len(args.upper_right) == 2

lower_left = args.lower_left 
upper_right = args.upper_right 
upper_left = args.upper_left 
lower_right = args.lower_right 

mesh = df.RectangleMesh(df.Point(0, 0), df.Point(1, 1), 1, 1)

V = df.FunctionSpace(mesh, 'DG', 1)
u = df.TrialFunction(V)
v = df.TestFunction(V)

# we want to restrict ourselves to one edge
sub_domain_interior = df.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
sub_domain_exterior = df.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
for edge in df.edges(mesh):
    v1, v2 = df.vertices(edge)
    if not np.isclose(v1.x(0), v2.x(0)) and not np.isclose(v1.x(1), v2.x(1)):
        sub_domain_interior[edge] = 1
    if np.isclose(v1.x(0), 0) and np.isclose(v2.x(0), 0): 
        sub_domain_exterior[edge] = 1

sub_domain_cells = df.MeshFunction("size_t", mesh, mesh.topology().dim())
# we mark the first cell:
# cell = df.cells(mesh).__next__()
cell = df.Cell(mesh, 1)
sub_domain_cells[cell] = 1

dS = df.Measure('dS', domain=mesh, subdomain_data=sub_domain_interior)
ds = df.Measure('ds', domain=mesh, subdomain_data=sub_domain_exterior)
dx = df.Measure('dx', domain=mesh, subdomain_data=sub_domain_cells)

mesh.coordinates()[0][:] = np.array(lower_left)
mesh.coordinates()[1][:] = np.array(lower_right)
mesh.coordinates()[2][:] = np.array(upper_left)
mesh.coordinates()[3][:] = np.array(upper_right)

n = df.FacetNormal(mesh)
h = df.CellDiameter(mesh)
kappad = df.Expression('2*x[0] - x[1] + 0.5', degree=1)
kappa1 = kappa2 = lambda x: 2*x[0] - x[1] + 0.5


def convert_matrix_to_dolfin(matrix):
    # we only support one rectangle with diagonal
    assert matrix.shape[0] == 6 and matrix.shape[1] == 6
    # dof mapping discovered experimentally
    dofmap_for_fenics = [4, 5, 3, 1, 2, 0]
    dolfin_matrix = np.zeros((6, 6))
    for idx1 in range(6):
        for idx2 in range(6):
            dolfin_idx1 = dofmap_for_fenics[idx1]
            dolfin_idx2 = dofmap_for_fenics[idx2]
            dolfin_matrix[dolfin_idx1, dolfin_idx2] = matrix[idx1, idx2]
    return dolfin_matrix


def convert_dolfin_to_local(matrix, filter_range=None):
    if filter_range is None:
        filter_range = np.array(list(range(6)))
    # we only support one rectangle with diagonal
    assert matrix.shape[0] == 6 and matrix.shape[1] == 6
    # dof mapping discovered experimentally
    dofmap_local = [5, 3, 4, 2, 0, 1]
    local_matrix = np.zeros((6, 6))
    for idx1 in range(6):
        for idx2 in range(6):
            local_idx1 = dofmap_local[idx1]
            local_idx2 = dofmap_local[idx2]
            local_matrix[local_idx1, local_idx2] = matrix[idx1, idx2]
    return (local_matrix[filter_range, :])[:, filter_range]

if args.compare_jump:
    print('[[u]]*[[v]]*dx:')
    print('in dolfin:')
    K_dolfin = df.assemble(df.inner(df.jump(u), df.jump(v))*dS(1))
    K_dolfin = convert_dolfin_to_local(K_dolfin.array(), filter_range=[0, 1, 3, 4])
    print(K_dolfin)
    print('here:')
    K_here = assemble_local_jump_interior(lower_left, upper_right)
    print(K_here)
    equal = np.allclose(K_dolfin, K_here)
    print('here:')
    K_here_gen = assemble_local_jump_interior_dangling([lower_left, upper_right, lower_left, upper_right])
    print(K_here_gen)
    equal &= np.allclose(K_dolfin, K_here_gen)
    if equal:
        print('matrices are equal')
    else:
        print('!matrices different!')

if args.compare_average:
    print('[[u]]*{{kappa(x)*grad(v)}}*dx:')
    print('in dolfin:')
    K_dolfin = df.assemble(df.inner(df.jump(u, n), df.avg(kappad*df.grad(v)))*dS(1))
    K_dolfin = convert_dolfin_to_local(K_dolfin.array())
    print(K_dolfin)
    print('here:')
    K_here = assemble_local_jump_avg_interior(lower_left, upper_right, upper_left, lower_right, kappa1, kappa2)
    print(K_here)
    equal = np.allclose(K_dolfin, K_here)
    print('here:')
    list_p = [lower_left, upper_right, upper_left, lower_left, upper_right, lower_right]
    K_here_gen = assemble_local_jump_avg_interior_dangling(list_p, kappa1, kappa2)
    print(K_here_gen)
    equal &= np.allclose(K_dolfin, K_here_gen)
    if equal:
        print('matrices are equal')
    else:
        print('!matrices different!')

if args.compare_symmetric:
    print('(-[[u]]*{{kappa(x)*grad(v)}}-{{kappa(x)*grad(u)}}*[[v]])*dS :')
    print('in dolfin:')
    form = 0 
    form += -df.inner(df.jump(u, n), df.avg(kappad*df.grad(v)))*dS(1)
    form += -df.inner(df.jump(v, n), df.avg(kappad*df.grad(u)))*dS(1)
    K_dolfin = df.assemble(form)
    K_dolfin = convert_dolfin_to_local(K_dolfin.array())
    print(K_dolfin)
    print('here:')
    K_here = assemble_local_minus_jump_avg_sym_interior(lower_left, upper_right, upper_left, lower_right, kappa1, kappa2)
    print(K_here)
    equal = np.allclose(K_dolfin, K_here)
    if equal:
        print('matrices are equal')
    else:
        print('!matrices different!')

if args.compare_boundary:
    print('(2/h*[[u]]*[[v]]-[[u]]*{{kappa(x)*grad(v)}}-{{kappa(x)*grad(u)}}*[[v]])*dS :')
    print('in dolfin:')
    form = 0 
    form += -df.inner(df.jump(u, n), df.avg(kappad*df.grad(v)))*dS(1)
    form += -df.inner(df.jump(v, n), df.avg(kappad*df.grad(u)))*dS(1)
    form += 2/df.avg(h)*df.inner(df.jump(u,n), df.jump(v,n))*dS(1)
    K_dolfin = df.assemble(form)
    K_dolfin = convert_dolfin_to_local(K_dolfin.array())
    print(K_dolfin)
    print('here:')
    K_here = assemble_local_boundary_integrals_interior(lower_left, upper_right, upper_left, lower_right, kappa1, kappa2)
    print(K_here)
    equal = np.allclose(K_dolfin, K_here)
    if equal:
        print('matrices are equal')
    else:
        print('!matrices different!')

if args.compare_rhs_mass:
    print('v*rhs*dx:')
    rhs = df.Expression('2*x[0] -1*x[1] +1+3*x[0]', degree=1)
    rhs_fct = lambda x: 2*x[0] -1*x[1] +1+3*x[0]
    form = v*rhs*dx(1)
    f_dolfin = df.assemble(form)[np.array([3,4,5])]
    print('in dolfin:')
    print(f_dolfin)
    f_here = assemble_local_mass_rhs(upper_left, lower_left, upper_right, rhs_fct)
    print('here:')
    print(f_here)
    equal = np.allclose(f_dolfin, f_here)
    if equal:
        print('vectors are equal')
    else:
        print('!vectors different!')

if args.plot_mesh:
    df.plot(mesh)
    plt.show()


def find_out_dof_positions():
    for idx in range(6):
        u = df.Function(V)
        u.vector()[idx] = 1
        print(idx)
        print(u.vector()[:])
        print('(1e-9, 0) {}'.format(u(1e-9, 0)))
        print('(1, 0) {}'.format(u(1, 0)))
        print('(1, 1-1e-9) {}'.format(u(1, 1-1e-9)))
        print('(0, 1e-9) {}'.format(u(0, 1e-9)))
        print('(0, 1) {}'.format(u(0, 1)))
        print('(1-1e-9, 1) {}'.format(u(1-1e-9, 1)))
        print('------')
        df.plot(u)
        plt.show()
    df.plot(mesh)
    plt.show()
