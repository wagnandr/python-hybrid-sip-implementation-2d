import os
import argparse
from math import floor
import numpy as np
from matplotlib import pyplot as plt

from example_singularity import (
    analytic_solution_fct,
    boundary_indicator,
    rhs,
    K_1, K_2, K_3, K_4,
    kappa_min, kappa_max
)
from example_singularity import run as run_uniform
from core.rectangle_mesh import create_rectangle_mesh
from core.mesh_topology import get_cell_midpoint
from core.solver import solve, RunMode, interpolate, save_vtk
from example_poisson import increase_refinement_multiplicatively
from core.section import section


class SquareMiddleRefinementIndicator:
    def __init__(self, n):
        self.n = n 
    
    def __call__(self, cell_mp):
        x = floor(self.n*(1-abs(cell_mp[0])))
        y = floor(self.n*(1-abs(cell_mp[1])))
        level = min(x,y)
        return 2**level


def run(n, run_mode):
    # create the connectivity of our mesh
    mesh_topology = create_rectangle_mesh(np.array([-1, -1]), np.array([1, 1]), 2*n, 1)

    increase_refinement_multiplicatively(mesh_topology, SquareMiddleRefinementIndicator(n))

    # fill the kappas
    list_kappa = []
    for cell_idx in range(mesh_topology.num_cells):
        mp = get_cell_midpoint(mesh_topology, cell_idx)
        if mp[0] >= 0 and mp[1] >= 0:
            list_kappa.append(lambda x: K_1)
        elif mp[0] <= 0 and mp[1] >= 0:
            list_kappa.append(lambda x: K_2)
        elif mp[0] <= 0 and mp[1] <= 0:
            list_kappa.append(lambda x: K_3)
        elif mp[0] >= 0 and mp[1] <= 0:
            list_kappa.append(lambda x: K_4)

    result = solve(
        mesh_topology, 
        RunMode.dg,
        analytic_solution_fct,
        boundary_indicator,
        rhs, 
        list_kappa, 
        kappa_min, 
        kappa_max,
        False)

    u, size, error, macro_cells = result

    if not os.path.exists('results'):
        os.makedirs('results')

    u_interp = np.zeros(size)
    interpolate(u_interp, macro_cells, analytic_solution_fct)
    error_fct = np.abs(u_interp - u)

    with section('writing solution to file'):
        filepath = 'results/singularity_dangling_solution_{}_{}.vtk'.format(run_mode, n)
        save_vtk(u, macro_cells, filepath)

    with section('writing analytic solution to file'):
        filepath = 'results/singularity_dangling_analytic_{}_{}.vtk'.format(run_mode, n)
        save_vtk(u_interp, macro_cells, filepath)

    with section('writing local error to file'):
        filepath = 'results/singularity_dangling_error_{}_{}.vtk'.format(run_mode, n)
        save_vtk(error_fct, macro_cells, filepath, name='error')
    
    return error, size


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run the Hybrid DG test problem.')
    parser.add_argument('--start-n', help='With which n should we start', type=int, default=1)
    parser.add_argument('--stop-n', help='With which n should we stop', type=int, default=5)
    parser.add_argument('--compare-p1', help='Compare with P1', action='store_true')
    parser.add_argument('--compare-hdg', help='Compare with HDG', action='store_true')
    parser.add_argument('-rm', '--run-mode', 
        help='Specifies which solver should be used, dg with and without continuous corners.', 
        choices=[RunMode.dg, RunMode.dg_continuous_corners],
        default=RunMode.dg)
    args = parser.parse_args()

    list_error = []
    list_m = []

    for n in range(args.start_n, args.stop_n+1):
        error, size = run(n, args.run_mode)
        list_m.append(size)     
        list_error.append(error)
        print(error)

    list_error_p1 = []
    list_error_hdg = []
    list_m_p1 = []
    list_m_hdg = []
    if args.compare_p1:
        m = 0
        last_size = 0
        while last_size < size:
            error, last_size = run_uniform(m, RunMode.p1)
            list_m_p1.append(last_size)     
            list_error_p1.append(error)
            m += 1
    if args.compare_hdg:
        m = 0
        last_size = 0
        while last_size < size:
            error, last_size = run_uniform(m, RunMode.dg)
            list_m_hdg.append(last_size)     
            list_error_hdg.append(error)
            m += 1

    plt.loglog(list_m, list_error[0]*list_m[0]**(0.5)/np.array(list_m)**(0.5), ':', label='slope -0.5')
    plt.loglog(list_m, list_error[0]*list_m[0]/np.array(list_m), ':', label='slope -1')

    if args.compare_p1:
        plt.loglog(list_m_p1[:-1], list_error_p1[:-1], '-.', label='P1')
    if args.compare_hdg:
        plt.loglog(list_m_hdg[:-1], list_error_hdg[:-1], '-x', label='HDG')

    plt.loglog(list_m, list_error, '-o', label='HDG (adaptive)')

    plt.xlabel('# dofs')
    plt.ylabel('error')
    plt.legend()
    plt.grid(True)
    plt.show()

