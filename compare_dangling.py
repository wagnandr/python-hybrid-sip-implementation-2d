"""
Utility functions for comparing the assembly routines with dangling nodes with the equally meshed implementations.
"""

import numpy as np

from core.assemble_local import (
    assemble_local_jump_avg_interior,
    assemble_local_jump_interior
)
from core.assemble_local_dangling import (
    assemble_local_jump_avg_interior_dangling,
    assemble_local_jump_interior_dangling
)


def contract_line_tensor(tensor):
    """
    Converts an element tensor of the configuration
        0    5    1
        +----+----+
        +----+----+
        2    3    4
    to an element tensor of the configuration
        0         1
        +---------+
        +----+----+
        2    3    4
    by eliminating the last element and thus reducing the tensor dimension.
    Note that the order is vital!
    """
    assert tensor.shape[0] == 6 and tensor.shape[1] == 6
    contracted = tensor[0:5, 0:5]
    for idx in range(2, 5):
        contracted[idx, 0] += 0.5*tensor[idx, 5]
        contracted[idx, 1] += 0.5*tensor[idx, 5]
        contracted[0, idx] += 0.5*tensor[5, idx]
        contracted[1, idx] += 0.5*tensor[5, idx]
    contracted[0, 0] += 0.5*tensor[0, 5] + 0.5*tensor[5, 0] + 0.25*tensor[5, 5]
    contracted[1, 0] += 0.5*tensor[1, 5] + 0.5*tensor[5, 0] + 0.25*tensor[5, 5]
    contracted[0, 1] += 0.5*tensor[0, 5] + 0.5*tensor[5, 1] + 0.25*tensor[5, 5]
    contracted[1, 1] += 0.5*tensor[1, 5] + 0.5*tensor[5, 1] + 0.25*tensor[5, 5]
    return contracted


def create_line_tensor_naive(p):
    """
    Creates a jump tensor with dofs
        0         1
        +---------+
        +----+----+
        2    3    4
    by assembling the tensors of
        0    5
        +----+
        +----+
        2    3
    as well as
        5    1
        +----+
        +----+
        3    4
    and eliminating the 5th dof.
    """
    K = np.zeros((6, 6))
    K_loc = assemble_local_jump_interior(p[0], p[5])
    dof_map = [0, 5, 2, 3]
    for idx_i in range(4):
        for idx_j in range(4):
            K[dof_map[idx_i], dof_map[idx_j]] += K_loc[idx_i, idx_j]
    K_loc = assemble_local_jump_interior(p[5], p[1])
    dof_map = [5, 1, 3, 4]
    for idx_i in range(4):
        for idx_j in range(4):
            K[dof_map[idx_i], dof_map[idx_j]] += K_loc[idx_i, idx_j]
    K = contract_line_tensor(K)
    return K


def contract_triangle_tensor(tensor):
    """
    Converts an element tensor of the configuration
             2
             +
            /|\
          /  |  \
        0/   |8  \1
        +----+----+
        +----+----+
        3\  /4\  /5
          \/   \/
          6     7
    to an element tensor of the configuration
             2
             +
            / \
          /     \
        0/       \1
        +---------+
        +----+----+
        3\  /4\  /5
          \/   \/
          6     7
    by eliminating the last element (the 8th dof) and thus reducing the tensor dimension.
    Note that the order is vital!
    """
    assert tensor.shape[0] == 9 and tensor.shape[1] == 9
    contracted = tensor[0:8, 0:8]
    for idx in range(2, 8):
        contracted[idx, 0] += 0.5*tensor[idx, 8]
        contracted[idx, 1] += 0.5*tensor[idx, 8]
        contracted[0, idx] += 0.5*tensor[8, idx]
        contracted[1, idx] += 0.5*tensor[8, idx]
    contracted[0, 0] += 0.5*tensor[0, 8] + 0.5*tensor[8, 0] + 0.25*tensor[8, 8]
    contracted[1, 0] += 0.5*tensor[1, 8] + 0.5*tensor[8, 0] + 0.25*tensor[8, 8]
    contracted[0, 1] += 0.5*tensor[0, 8] + 0.5*tensor[8, 1] + 0.25*tensor[8, 8]
    contracted[1, 1] += 0.5*tensor[1, 8] + 0.5*tensor[8, 1] + 0.25*tensor[8, 8]
    return contracted


def create_triangle_tensor_naive(p, f):
    """
    Creates a jump average tensor with dofs
             2
             +
            / \
          /     \
        0/       \1
        +---------+
        +----+----+
        3\  /4\  /5
          \/   \/
          6     7
    by assembling the tensors of
             2
             +
            /|\
          /  |  \
        0/   |8  \1
        +----+----+
    as well as
        +----+----+
        3\  /4\  /5
          \/   \/
          6     7
        5    1
    and eliminating the 8th dof.
    """
    K = np.zeros((9, 9))
    K_loc = assemble_local_jump_avg_interior(p[0], p[4], p[2], p[6], f, f)
    dof_map = [0, 8, 2, 3, 4, 6]
    for idx_i in range(6):
        for idx_j in range(6):
            K[dof_map[idx_i], dof_map[idx_j]] += K_loc[idx_i, idx_j]
    K_loc = assemble_local_jump_avg_interior(p[4], p[1], p[2], p[7], f, f)
    dof_map = [8, 1, 2, 4, 5, 7]
    for idx_i in range(6):
        for idx_j in range(6):
            K[dof_map[idx_i], dof_map[idx_j]] += K_loc[idx_i, idx_j]
    K = contract_triangle_tensor(K)
    return K


def _test():
    """ Test for a simple geometry. """
    f = lambda x: 1

    p = [(0, 0), (0, 1), (0, 0), (0, 0.5), (0, 1), (0, 0.5)]
    K = create_line_tensor_naive(p)
    p = p[:-1]
    K_2 = assemble_local_jump_interior_dangling(p)
    print('----')
    print(K)
    print()
    print(K_2)
    if np.allclose(K, K_2):
        print('tensors equal')
    else:
        print('!tensors different')
    print('----')

    p = [(0, 0), (0, 1), (-0.5, 0.5), (0, 0), (0, 0.5), (0, 1), (0.25, 0.25), (0.25, 0.75), (0, 0.5)]
    K = create_triangle_tensor_naive(p, f)
    p = p[:-1]
    K_2 = assemble_local_jump_avg_interior_dangling(p, f, f)
    print('----')
    print(K)
    print()
    print(K_2)
    if np.allclose(K, K_2):
        print('tensors equal')
    else:
        print('!tensors different')
    print('----')


if __name__ == '__main__':
    _test()
