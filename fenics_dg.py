"""
Test implementation to get a feeling how the DG-Method works
"""

import os
import argparse
import dolfin as df


parser = argparse.ArgumentParser(description='Runs a DG implementation of the laplace equation in fenics.')
parser.add_argument('-N', '--grid-size', help='Creates a NxN grid for the problem.', type=int, default=8)
parser.add_argument('-o', '--output-directory', help='Where to output the solution?', type=str, default='results')
parser.add_argument('-pm', '--print-matrix', help='Prints the matrix and rhs.', action='store_true')
args = parser.parse_args()

boundary_indicator = df.CompiledSubDomain('on_boundary')
boundary_value = df.Expression('2*x[0]+1+3*x[1]', degree=1)

N = args.grid_size
mesh = df.RectangleMesh(df.Point(0,0), df.Point(1,1), N, N)
V = df.FunctionSpace(mesh,'DG',1)
u = df.TrialFunction(V)
v = df.TestFunction(V)

boundary = df.MeshFunction('size_t', mesh, mesh.topology().dim()-1)
boundary_indicator.mark(boundary, 1)
ds = df.Measure('ds', domain=mesh, subdomain_data=boundary)

n = df.FacetNormal(mesh)
h = df.CellDiameter(mesh)

a = 0 
a += df.inner(df.grad(u), df.grad(v))*df.dx
a += -df.inner(df.jump(u, n), df.avg(df.grad(v)))*df.dS
a += -df.inner(df.jump(v, n), df.avg(df.grad(u)))*df.dS
a += 2/df.avg(h)*df.inner(df.jump(u, n), df.jump(v, n))*df.dS
a += -df.dot(df.grad(v), u*n)*ds(1)
a += -df.dot(v*n, df.grad(u))*ds(1)
a += 2/h*df.dot(v, u)*ds(1)

l = 0
l += df.Constant(0)*v*df.dx
l += -df.inner(df.grad(v), n)*boundary_value*ds(1)
l += +2/h*df.dot(v, boundary_value)*ds(1)

u_sol = df.Function(V, name='u')

A = df.assemble(a)
b = df.assemble(l)
solver = df.LUSolver()
solver.solve(A, u_sol.vector(), b)
# df.solve(a == l, u_sol, [])

if not os.path.exists(args.output_directory):
    os.makedirs(args.output_directory)

with df.XDMFFile(os.path.join(args.output_directory, 'fenics_dg.xdmf')) as f:
    f.write(u_sol)

if args.print_matrix:
    print('Matrix:')
    print(A.array())
    print('-------')
    print('Rhs:')
    print(b[:])
    print('-------')

print('u_sol(0.5, 0.5) = {} (should be 3.5)'.format(u_sol(0.5, 0.5)))