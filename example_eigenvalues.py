import sys
import os
import argparse
import slepc4py
from slepc4py import SLEPc
import petsc4py
from petsc4py import PETSc
import numpy as np

from core.solver import (
    initialize_macro_cells,
    initialize_independent_dofs,
    initialize_continuous_dofs,
    assemble_cells,
    assemble_cells_mass,
    assemble_interior_integrals,
    multiply_with_inverse_diagonal,
    calculate_max_jump,
    save_vtk
)
from core.simple_vtk_ascii_writer import SimplePVDWriter
from core.rectangle_mesh import create_rectangle_mesh


slepc4py.init(sys.argv)
petsc4py.init(sys.argv)


def _boundary_indicator(p):
    return np.isclose(p[0], 0) or np.isclose(p[1], 0) or np.isclose(p[0], 1) or np.isclose(p[1], 1)


class EigenvalueCalculator:
    def __init__(self, n, m, sigma, use_mass, impose_continuity, inverse_diagonal):
        self.n = n 
        self.m = m 
        self.sigma = sigma
        self.use_mass = use_mass
        self.impose_continuity = impose_continuity
        self.inverse_diagonal = inverse_diagonal

    def setup(self):
        n, m = self.n, self.m

        list_kappa = [lambda x: 1 for x in range(4*n*n)]
        boundary_value = lambda x: 0
        rhs = lambda x: 0
        verbose = False 
        kappa_min = kappa_max = 1

        # we have NxN cells
        N = 2**m 

        # create the connectivity of our mesh
        self.mesh_topology = create_rectangle_mesh(np.array([0, 0]), np.array([1, 1]), n, N)

        self.macro_cells = initialize_macro_cells(self.mesh_topology)

        if self.impose_continuity:
            next_dof = initialize_continuous_dofs(self.mesh_topology, self.macro_cells, just_corners=False)
        else:
            next_dof = initialize_independent_dofs(self.mesh_topology, self.macro_cells)

        #for mc in self.macro_cells:
        #    for idx, c in enumerate(mc.vertex_to_coord_map):
        #        print(c, mc.vertex_to_dof_map[idx])
        #exit()

        self.size = size = next_dof

        self.K = PETSc.Mat().createAIJ(comm=PETSc.COMM_SELF, size=size, nnz=1000)
        self.K.setUp()
        u, f = self.K.createVecs()

        assemble_cells(self.K, f, self.macro_cells, list_kappa, boundary_value, rhs, verbose)

        if not self.impose_continuity:
            assemble_interior_integrals(self.K, self.mesh_topology, self.macro_cells, list_kappa, kappa_min, kappa_max, verbose, self.sigma)

        self.K.assemble()

        if self.inverse_diagonal:
            self.K = multiply_with_inverse_diagonal(self.K)
            self.K.assemble()

        self.M = None
        if self.use_mass:
            self.M = PETSc.Mat().createAIJ(comm=PETSc.COMM_SELF, size=size, nnz=1000)
            self.M.setUp()
            assemble_cells_mass(self.M, self.macro_cells)
            self.M.assemble()
    
    def solve(self, num=None):
        if num is None:
            num = self.size

        self.E = SLEPc.EPS()
        self.E.create()

        self.E.setOperators(self.K, self.M)
        problemType = SLEPc.EPS.ProblemType.NHEP if self.M is None else SLEPc.EPS.ProblemType.GNHEP
        self.E.setProblemType(problemType)
        self.E.setDimensions(nev=num)
        self.E.setFromOptions()

        self.E.solve()
    
    def get_eigenvalue(self, idx):
        k = self.E.getEigenvalue(idx)
        return k.real

    def save(self, folder):
        eps_type = self.E.getType()
        print("solution method: {}".format(eps_type))
        nconv = self.E.getConverged()
        print("number of converged eigenpairs {}".format(nconv))

        pvd_writer = SimplePVDWriter()

        if nconv > 0:
            # Create the results vectors
            vr, wr = self.K.getVecs()
            vi, wi = self.K.getVecs()

            for idx in range(nconv-1, -1, -1):
                k = self.E.getEigenpair(idx, vr, vi)
                error = self.E.computeError(idx)

                name = 'eigenvector'
                if self.use_mass:
                    name += '_mass'
                if self.impose_continuity:
                    name += '_cont'
                if self.inverse_diagonal:
                    name += '_invdiag'
                path = os.path.join(folder, '{}{}{}_{}.vtu'.format(name, self.m, self.n, idx))
                save_vtk(vr, self.macro_cells, path, name='eigenvector')
                pvd_writer.add(path, k.real)

                if k.imag != 0.0:
                    print(" %9f%+9f j %12g" % (k.real, k.imag, error))
                else:
                    print("idx=%d real= %12f    error=  %12g" % (idx, k.real, error))

                max_jump = calculate_max_jump(vr, self.mesh_topology, self.macro_cells)
                print('  max jump = {}'.format(max_jump))

        pvd_writer.write(os.path.join(folder, '{}{}{}.pvd'.format(name, self.m, self.n)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run the Hybrid DG test problem.')
    parser.add_argument('-n', help='Use a nxn macro grid.', type=int, default=1)
    parser.add_argument('-m', help='Use triangle cells with 2^m vertices on each side.', type=int, default=1)
    parser.add_argument('--sigma', help='Penalty value sigma. If no value is provided, the default one is used.', type=float, required=False)
    parser.add_argument('--use-mass', help='We solve a generalized eigen problem with the mass matrix.', action='store_true')
    parser.add_argument('--impose-continuity', help='Use continuous basis functions.', action='store_true')
    parser.add_argument('--inverse-diagonal', help='Multiply with the inverse diagonal before.', action='store_true')
    args = parser.parse_args()

    calc = EigenvalueCalculator(
        args.n, args.m, args.sigma, 
        args.use_mass, args.impose_continuity, args.inverse_diagonal)
    calc.setup()
    calc.solve()
    calc.save('results')
