"""
Example from Riviere section 2.10.2.
"""

import os
from math import atan2, sqrt, sin, cos, pow, pi
import argparse
import numpy as np
from matplotlib import pyplot as plt

from core.rectangle_mesh import create_rectangle_mesh
from core.solver import (
    solve,
    RunMode,
    interpolate,
    save_vtk
)
from core.section import section


a_1 = 0.4472135955
a_2 = -0.7453559925
a_3 = -0.9441175905
a_4 = -2.401702643
b_1 = 1
b_2 = 2.333333333
b_3 = 0.55555555555
b_4 = -0.4814814814
delta = 0.5354409456

K_1 = K_3 = 5
K_2 = K_4 = 1


def analytic_solution_fct(p):
    x, y = p[0], p[1]
    # cartesian to polar coordinates
    r = sqrt(x**2 + y**2)
    theta = atan2(y, x) 
    theta = 2*pi+theta if theta < 0 else theta
    # choose the correct coefficients based on the quadrant
    if x >= 0 and y >= 0:
        a, b = a_1, b_1
    elif x <= 0 and y >= 0:
        a, b = a_2, b_2
    elif x <= 0 and y <= 0:
        a, b = a_3, b_3
    elif x >= 0 and y <= 0:
        a, b = a_4, b_4
    # evaluate
    return pow(r, delta)*(a*sin(delta*theta) + b*cos(delta*theta))


kappa1 = kappa2 = lambda p: K_3 
kappa3 = kappa4 = lambda p: K_4
kappa5 = kappa6 = lambda p: K_2
kappa7 = kappa8 = lambda p: K_1
list_kappa = [kappa1, kappa2, kappa3, kappa4, kappa5, kappa6, kappa7, kappa8]
kappa_min = min(K_1, K_2, K_3, K_4)
kappa_max = max(K_1, K_2, K_3, K_4)

rhs = lambda p: 0


def boundary_indicator(p):
    return np.isclose(p[0], -1) or np.isclose(p[1], -1) or np.isclose(p[0], 1) or np.isclose(p[1], 1)


def run(m, run_mode):
    N = 2**m 

    # create the connectivity of our mesh
    mesh_topology = create_rectangle_mesh(np.array([-1, -1]), np.array([1, 1]), 2, N)

    # from mesh_topology import draw_mesh
    # draw_mesh(mesh_topology)

    result = solve(
        mesh_topology, 
        run_mode,
        analytic_solution_fct,
        boundary_indicator,
        rhs, 
        list_kappa, 
        kappa_min, 
        kappa_max,
        False)
    u, size, error, macro_cells = result

    if not os.path.exists('results'):
        os.makedirs('results')

    u_interp = np.zeros(size)
    interpolate(u_interp, macro_cells, analytic_solution_fct)
    error_fct = np.abs(u_interp - u)

    with section('writing solution to file'):
        filepath = 'results/singularity_solution_{}_{}.vtk'.format(run_mode, m)
        save_vtk(u, macro_cells, filepath)

    with section('writing analytic solution to file'):
        filepath = 'results/singularity_analytic_{}_{}.vtk'.format(run_mode, m)
        save_vtk(u_interp, macro_cells, filepath)

    with section('writing local error to file'):
        filepath = 'results/singularity_error_{}_{}.vtk'.format(run_mode, m)
        save_vtk(error_fct, macro_cells, filepath, name='error')

    vertex_index = int(len(macro_cells[0].vertex_to_dof_map)/2)
    dof = macro_cells[0].vertex_to_dof_map[vertex_index]
    vcoord = macro_cells[0].vertex_to_coord_map[vertex_index]
    expected = analytic_solution_fct(vcoord)
    section.info('calculated u({}, {}) = {}, expected {}'.format(vcoord[0], vcoord[1], u[dof], expected))

    return error, size


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run the Hybrid DG test problem.')
    parser.add_argument('--start-m', help='With which m should we start', type=int, default=0)
    parser.add_argument('--stop-m', help='With which m should we stop', type=int, default=5)
    args = parser.parse_args()

    list_error_p1 = []
    list_error_dg = []
    list_m = []

    for m in range(args.start_m, args.stop_m+1):
        list_m.append(2*2**m)     # 2*2**m = number of intervals
        error, _ = run(m, RunMode.p1)
        list_error_p1.append(error)
        error, _ = run(m, RunMode.dg)
        list_error_dg.append(error)
        print(error)

    plt.loglog(list_m, list_error_p1, '-x', label='P1')
    plt.loglog(list_m, list_error_dg, '-o', label='HDG')
    plt.loglog(list_m, 5e-1/np.array(list_m)**1, ':', label='slope 1')
    plt.loglog(list_m, 4e-1/np.array(list_m)**(1.06), ':', label='slope 1.06')
    plt.xlabel('# intervals along one axis')
    plt.ylabel('error')
    plt.legend()
    plt.grid(True)
    plt.show()
