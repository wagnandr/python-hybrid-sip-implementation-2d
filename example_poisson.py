import sys
from math import pi
import os
import argparse
import petsc4py
import numpy as np

from core.macrocell import plot_macro_cell
from core.solver import (
    solve,
    save_vtk,
    interpolate,
    RunMode)
from core.mesh_topology import MeshTopology, get_cell_midpoint
from core.section import section
from core.rectangle_mesh import create_rectangle_mesh


petsc4py.init(sys.argv)


def _boundary_indicator(p):
    return np.isclose(p[0], 0) or np.isclose(p[1], 0) or np.isclose(p[0], 1) or np.isclose(p[1], 1)


def increase_refinement_multiplicatively(mesh_topology: MeshTopology, refinement_indicator):
    for cell_idx in range(mesh_topology.num_cells):
        midpoint = get_cell_midpoint(mesh_topology, cell_idx)
        factor = refinement_indicator(midpoint)
        mesh_topology.cell_to_refinement_level[cell_idx] *= factor


def lower_triangle_indicator(cell_mp):
    return cell_mp[0] > cell_mp[1]


class LowerTriangleRefinementIndicator:
    def __init__(self, factor):
        self.factor = factor
    
    def __call__(self, cell_mp):
        if lower_triangle_indicator(cell_mp):
            return self.factor
        else:
            return 1


def run(
    n, m, 
    plot_grids, 
    verbose, 
    run_mode, 
    boundary_value, 
    rhs, 
    kappa, kappa_min, kappa_max, 
    refinement_indicator, 
    name
):
    """
    MC1        1
    (0)  2 +---++ 1 (3)
           |  //|
           | // |
           |//  |
    (1)  0 ++---+ 0 (2)
            2       MC2
    The upper triangle is the first macro cell, the lower corresponds to the second one.
    """
    # we use everywhere the same kappa
    list_kappa = [kappa for x in range(4*n*n)]

    # we have NxN cells
    N = 2**m 

    # create the connectivity of our mesh
    mesh_topology = create_rectangle_mesh(np.array([0, 0]), np.array([1, 1]), n, N)
    increase_refinement_multiplicatively(mesh_topology, refinement_indicator)
    '''
    mesh_topology = MeshTopology()
    v0 = mesh_topology.add_vertex(np.array([0,1]))
    v1 = mesh_topology.add_vertex(np.array([0,0]))
    v2 = mesh_topology.add_vertex(np.array([1,0]))
    v3 = mesh_topology.add_vertex(np.array([1,1]))
    e13 = mesh_topology.add_edge(v1, v3)
    e03 = mesh_topology.add_edge(v3, v0)
    e01 = mesh_topology.add_edge(v0, v1)
    c0 = mesh_topology.add_cell(N, e13, e03, e01)
    e23 = mesh_topology.add_edge(v2, v3)
    e12 = mesh_topology.add_edge(v1, v2)
    c1 = mesh_topology.add_cell(N, e23, e13, e12)
    '''

    result = solve(mesh_topology, run_mode, boundary_value, _boundary_indicator, rhs, list_kappa, kappa_min, kappa_max,
                   verbose)
    u, size, error, macro_cells = result

    if not os.path.exists('results'):
        os.makedirs('results')

    u_interp = np.zeros(size)
    interpolate(u_interp, macro_cells, boundary_value)
    error_fct = np.abs(u_interp - u)

    with section('writing solution to file'):
        filepath = 'results/poisson_{}_solution_{}_{}.vtk'.format(name, n, m)
        save_vtk(u, macro_cells, filepath, name='solution')

    with section('writing analytic solution to file'):
        filepath = 'results/poisson_{}_analytic_solution_{}_{}.vtk'.format(name, n, m)
        save_vtk(u_interp, macro_cells, filepath, name='solution')

    with section('writing local error to file'):
        filepath = 'results/poisson_{}_error_{}_{}.vtk'.format(name, n, m)
        save_vtk(error_fct, macro_cells, filepath, name='error')

    vertex_index = int(len(macro_cells[0].vertex_to_dof_map)/2)
    dof = macro_cells[0].vertex_to_dof_map[vertex_index]
    vcoord = macro_cells[0].vertex_to_coord_map[vertex_index]
    expected = boundary_value(vcoord)
    section.info('calculated u({}, {}) = {}, expected {}'.format(vcoord[0], vcoord[1], u[dof], expected))

    if plot_grids:
        for macro_cell in macro_cells:
            plot_macro_cell(macro_cell)
    
    return error, size


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run the Hybrid DG test problem.')
    parser.add_argument('-n', help='Use a nxn macro grid.', type=int, default=1)
    parser.add_argument('-m', help='Use triangle cells with 2^m vertices on each side.', type=int, default=1)
    parser.add_argument('--refinement-factor', help='Which refinement should be applied to the lower triangle?.', type=int, default=1)
    parser.add_argument('-pg', '--plot-grids', help='Plots the created grids with numbers.', action='store_true')
    parser.add_argument('-v', '--verbose', help='Plots the created grids with numbers.', action='store_true')
    parser.add_argument('-rm', '--run-mode', 
        help='Specifies which solver should be used, p1 or hdg.', 
        choices=[RunMode.p1, RunMode.dg, RunMode.dg_continuous_corners],
        default=RunMode.p1)
    parser.add_argument('-o3', '--order-3', help='The solution is a polynomial of order 3, not 1.', action='store_true')
    args = parser.parse_args()

    # get postfix to identify solver in the output
    if args.run_mode == RunMode.dg:
        name_postfix = 'dg'
    elif args.run_mode == RunMode.p1:
        name_postfix = 'p1'
    elif args.run_mode == RunMode.dg_continuous_corners:
        name_postfix = 'dg-continuous-corners'

    if args.order_3:
        boundary_value = lambda x: np.sin(2*pi*x[0])*np.cos(3*pi*x[1])
        rhs = lambda x: (4+9)*pi**2*np.sin(2*pi*x[0])*np.cos(3*pi*x[1])
        kappa = lambda x: 1
        kappa_min = kappa_max = 1
        # boundary_value = lambda x: 2*x[0]*x[0]*x[0]+3-3*x[1]*x[0]
        # rhs = lambda x: -2*3*2*x[0]
        name = 'order_3_' + name_postfix 
    else:
        boundary_value = lambda x: 2*x[0]+3-3*x[1]
        rhs = lambda x: 0
        kappa = lambda x: 1
        kappa_min = kappa_max = 1
        name = 'order_1_' + name_postfix 
    run(args.n, args.m, 
        args.plot_grids, 
        args.verbose, 
        args.run_mode, 
        boundary_value, 
        rhs, 
        kappa, kappa_min, kappa_max, 
        LowerTriangleRefinementIndicator(args.refinement_factor),
        name)
