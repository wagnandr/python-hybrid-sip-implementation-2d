# About
A small collection of example scripts for a hybrid symmetric interior penalty (SIP) method in 2D implemented for a master's thesis.

These scripts were the basis of an implementation in HyTeG and helped to investigate the spectral properties of the involved operators.

# Prerequisits
The implementation depends on the following python libraries.
All scripts need
- numpy
- matplotlib
- petsc4py

The eigenvalue calculation depends in addition on
- slepc4py

For some scripts validating the assembly routines and comparing the hybrid SIP eigenvalues with the conformal P1 eigenvalues we need
- fenics 2019.1

# Structure
All the python scripts in this folder can be executed. A list of parameters can be queried with the ```-h``` option.
The ```core``` folder contains the actual implementation of the method.

